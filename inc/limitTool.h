#ifndef LIMITTOOL_H
#define LIMITTOOL_H

/*
Author: Aaron Armbruster
Date:   2012-05-25
Email:  armbrusa@umich.edu
Description: Script to run asymptotic CLs.

--------
00-01-00
-First version with updated bands

--------
00-01-01
-Fixed problem in asimov data creation that affected +1,2sigma bands

--------
00-01-02
-(Re)added support for non-sim pdfs (still need to be extended)
-Fixed default doFit arg of makeAsimovData
-Added better output for unresolved fit failures
-Improved retry loop for fit failures

--------
00-01-03
-#include <iomanip>

--------
Git migration
-Add missing factor of 1/2 in derivative of CLb (only affects convergence speed by small amount)
-Fix '==' assignment in doPredictiveFit (not used by default)
-Add dummy runAsymptoticsCLs() function for compilation
-Small changes for warningless compilation

/////////////////////
//////PREAMBLE///////
/////////////////////

The script uses an iterative process to find the crossing of qmu with the qmu95(mu/sigma) curve,
where qmu95(mu/sigma) is found assuming asymptotic formula for the distribution of the
test statistic f(qmu|mu') (arxiv 1007.1727) and of the test statistic qmu (or tilde)

The sequence is

mu_i+1 = mu_i - gamma_i*(mu_i - mu'_i)

where gamma_i is a dynamic damping factor used for convergence (nominal gamma_i = 1), and mu'_i is
determined by extrapolating the test statistic to the qmu95 curve assuming qmu is parabolic:

qmu'_i = (mu'_i - muhat)^2 / sigma_i^2 = qmu95(mu'_i / sigma_i)

where sigma_i is determined by computing qmu_i (not '):

sigma_i = (mu_i - muhat) / sqrt(qmu_i)

At the crossing qmu_N = qmu95 the assumption that qmu is a parabola goes away,
so we're not ultimately dependent on this assumption beyond its use in the asymptotic formula.

The sequence ends when the relative correction factor gamma*(mu_i - mu'_i) / mu_i is less than some
specified precision (0.005 by default)




///////////////////////////
//////AFTER RUNNING////////
///////////////////////////


The results will be printed as well as stored in a root file in the folder 'root-files/<folder>', where <folder>
is specified by you (default 'test')

The root file has a 7-bin TH1D named 'limit', where each bin is filled with the upper limit values in this order:

1: Observed
2: Median
3: +2 sigma
4: +1 sigma
5: -1 sigma
6: -2 sigma
7: mu=0 fit status (only meaningful if asimov data is generated within the macro)

It will also store the result of the old bands procedure in a TH1D named 'limit_old'.




//////////////////////////


This version is functionally fully consistent with the previous tag.

NOTE: The script runs significantly faster when compiled
*/

#include "TMath.h"
#include "Math/ProbFuncMathCore.h"
#include "Math/QuantFuncMathCore.h"
#include "TFile.h"
#include "TH1D.h"
#include "TGraph.h"

#include "RooWorkspace.h"
#include "RooNLLVar.h"
#include "RooStats/ModelConfig.h"
#include "RooDataSet.h"
#include "RooRealVar.h"
#include "Math/MinimizerOptions.h"
#include "TStopwatch.h"
#include "RooMinimizer.h"
#include "RooCategory.h"
#include "RooSimultaneous.h"
#include "RooProduct.h"

#include <map>
#include <iostream>
#include <sstream>
#include <iomanip>

#include "fitTool.h"
#include "auxUtils.h"

using namespace RooFit;
using namespace RooStats;

class limitTool
{
private:
  // band configuration
  bool betterBands;           // (recommendation = 1) improve bands by using a more appropriate asimov dataset for those points
  bool betterNegativeBands;   // (recommendation = 0) improve also the negative bands
  bool profileNegativeAtZero; // (recommendation = 0) profile asimov for negative bands at zero

  // other configuration
  string defaultMinimizer;  // or "Minuit"
  int defaultPrintLevel;    // Minuit print level
  int defaultStrategy;      // Minimization strategy. 0-2. 0 = fastest, least robust. 2 = slowest, most robust
  bool killBelowFatal;      // In case you want to suppress RooFit warnings further, set to 1
  bool doBlind;             // in case your analysis is blinded
  bool conditionalExpected; // Profiling mode for Asimov data: 0 = conditional MLEs, 1 = nominal MLEs
  int doTilde;              // Whether bound mu at zero. If doTilde > 0 do the \tilde{q}_{mu} asymptotics. For doTilde = 1, will first run unconditional fit allowing mu go negative. For doTilde = 2, will force mu > 0 in the unconditional fit
  bool doExp;               // compute expected limit
  bool doObs;               // compute observed limit
  double precision;         // % precision in mu that defines iterative cutoff
  bool verbose;             // 1 = very spammy
  bool usePredictiveFit;    // experimental, extrapolate best fit nuisance parameters based on previous fit results
  bool extrapolateSigma;    // experimantal, extrapolate sigma based on previous fits
  int maxRetries;           // number of minimize(fcn) retries before giving up

  // don't touch!
  std::map<RooNLLVar *, double> map_nll_muhat;
  std::map<RooNLLVar *, double> map_muhat;
  std::map<RooDataSet *, RooNLLVar *> map_data_nll;
  std::map<RooNLLVar *, std::string> map_snapshots;
  std::map<RooNLLVar *, std::map<double, double>> map_nll_mu_sigma;
  RooWorkspace *w;
  ModelConfig *mc;
  RooDataSet *data;
  RooRealVar *_firstPOI;
  RooNLLVar *asimov_0_nll;
  RooNLLVar *obs_nll;
  int nrMinimize;
  int direction;
  int global_status;
  double target_CLs;
  fitTool *_fitter;
  double _initial_guess;

public:
  // for compilation
  limitTool(RooWorkspace *w, ModelConfig *mc, RooDataSet *data, fitTool *fitter = NULL, RooRealVar *firstPOI = NULL, double initial_guess = 1);

  // main
  void runAsymptoticsCLs(string outputFileName, const char *asimovDataName = "", double CL = 0.95);

  double getLimit(RooNLLVar *nll, double initial_guess = 0);
  double getSigma(RooNLLVar *nll, double mu, double muhat, double &qmu);
  double getQmu(RooNLLVar *nll, double mu);
  void saveSnapshot(RooNLLVar *nll, double mu);
  void loadSnapshot(RooNLLVar *nll, double mu);
  void doPredictiveFit(RooNLLVar *nll, double mu1, double m2, double mu);
  RooNLLVar *createNLL(RooDataSet *_data);
  double getNLL(RooNLLVar *nll);
  double findCrossing(double sigma_obs, double sigma, double muhat);
  void setMu(double mu);
  double getQmu95_brute(double sigma, double mu);
  double getQmu95(double sigma, double mu);
  double calcCLs(double qmu_tilde, double sigma, double mu);
  double calcPmu(double qmu_tilde, double sigma, double mu);
  double calcPb(double qmu_tilde, double sigma, double mu);
  double calcDerCLs(double qmu, double sigma, double mu);
  int minimize(RooNLLVar* nll);
  void setDoExp(bool flag) { doExp = flag; }
  void setDoObs(bool flag) { doObs = flag; }

  void unfoldConstraints(RooArgSet &initial, RooArgSet &final, RooArgSet &obs, RooArgSet &nuis, int &counter);
  RooDataSet *makeAsimovData(bool doConditional, RooNLLVar *conditioning_nll, double mu_val, string *mu_str = NULL, string *mu_prof_str = NULL, double mu_val_profile = -999, bool doFit = true);
  void setBetterBands(bool flag) { betterBands = flag; }
  void setTestStatisticMode(int mode) { doTilde = mode; }
  void setBlind(bool flag){doBlind=flag; conditionalExpected = 1 && !doBlind; doObs = 1 && !doBlind; }
};

#endif

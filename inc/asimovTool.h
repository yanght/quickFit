#ifndef ASIMOVTOOL_HEADER
#define ASIMOVTOOL_HEADER

#include "CommonHead.h"
#include "RooFitHead.h"
#include "RooStatsHead.h"

#include "utils.h"
#include "auxUtils.h"
#include "fitTool.h"

using namespace std;
using namespace RooFit;
using namespace RooStats;

class asimovTool{
private:
  vector<TString> _asimovNames, _asimovSetups, _asimovProfiles;
  vector<TString> _SnapshotsAll, _SnapshotsNuis, _SnapshotsGlob, _SnapshotsPOI, _Snapshots;
  vector<TString> _injectionFiles;
  TString _rangeName;
  fitTool *_fitter;
  // action items
  static TString RAW;
  static TString FIT;
  static TString RESET;
  static TString GENASIMOV;
  static TString FLOAT;
  static TString FIXSYST;
  static TString MATCHGLOB;
  static TString RESETNUIS;
  static TString SAVESNAPSHOT;
public:
  asimovTool(fitTool *fitter){_fitter=fitter;}
  void addEntry(TXMLNode *node);
  void generateAsimov(ModelConfig *mc, TString dataName);
  void printSummary();
  bool genAsimov(){return _asimovNames.size()>0;}
  void setRange(TString rangeName){_rangeName=rangeName;}
  void matchGlob(ModelConfig *mc);
  void resetNuis(ModelConfig *mc);
};

#endif

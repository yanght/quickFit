#ifndef FITTOOL_HEADER
#define FITTOOL_HEADER

#include "CommonHead.h"
#include "RooFitHead.h"
#include "RooStatsHead.h"
#include "utils.h"

using namespace RooFit;
using namespace RooStats;

class fitTool
{
private:
  TString _minAlgo, _outputFile, _ssName, _rangeName;
  float _minTolerance, _errorLevel, _samplingRelTol;
  bool _nllOffset, _useHESSE, _useSIMPLEX, _saveWS, _fixStarCache, _saveErrors, _saveFitResult, _GKIntegrator, _saveNP, _doSumW2, _useBatch, _saveHesse, _fitHesse;
  int _minStrat, _optConst, _printLevel, _nCPU, _useMINOS;
  RooArgSet *_externalConstraint, *_paramList;
  int _status;
  std::unique_ptr<RooFitResult> _result;

public:
  fitTool();

  // Options
  void useHESSE(bool flag) { _useHESSE = flag; }                            // Enable Hesse
  void useMINOS(int flag) { _useMINOS = flag; }                             // Minos mode
  void useSIMPLEX(bool flag) { _useSIMPLEX = flag; }                        // Enable simplex
  void setNLLOffset(bool flag) { _nllOffset = flag; }                       // Enable NLL offset
  void saveWorkspace(bool flag) { _saveWS = flag; }                         // Enable workspace saving
  void saveErrors(bool flag) { _saveErrors = flag; }                        // Enable error saving in output file
  void saveFitResult(bool flag) { _saveFitResult = flag; }                  // Enable saving of fit results
  void setFixStarCache(bool flag) { _fixStarCache = flag; }                 // Enable fixing of RooStarMomentMorph
  void setTolerance(float val) { _minTolerance = val; }                     // Set minimizer tolerance
  void setNCPU(int val) { _nCPU = val; }                                    // Set number of CPUs to be used (RooFit cannot handle multi-threading for the time being)
  void setStrategy(int val) { _minStrat = val; }                            // Set Migrad strategy
  void setOptConst(int val) { _optConst = val; }                            // Set constant optimization strategy
  void setPrintLevel(int val) { _printLevel = val; }                        // Set printout level
  void setOutputFile(TString str) { _outputFile = str; }                    // Set output file name
  void setSnapshotName(TString str) { _ssName = str; }                      // Set snapshot name
  void setMinAlgo(TString str) { _minAlgo = str; }                          // Set minimization algorithm
  void setExternalConstraint(RooArgSet *set) { _externalConstraint = set; } // Set external constraint
  void setParamList(RooArgSet *set) { _paramList = set; }                   // Set list of parameters of interest
  void setGKIntegrator(bool flag) { _GKIntegrator = flag; }                 // Enable Gauss-Kronrod integrator
  void setErrorLevel(float level) { _errorLevel = level; }                  // Set error level
  void setSaveNP(bool flag) { _saveNP = flag; }                             // Enable saving of nuisance parameter fit results in TTree
  void setSumW2(bool flag) { _doSumW2 = flag; }                             // Enable sum-of-weight-squared correction of error
  void setBatchMode(bool flag) { _useBatch = flag; }                        // Enable batch mode
  void setSamplingRelTol(float val) { _samplingRelTol = val; }              // Set sampling relative tolerance
  void setStorageLevel(int level);                                          // Set storage level (avoid memory leakage in the fit)
  void setRangeName(TString rangeName) { _rangeName = rangeName; }          // Set fit range name
  void setSaveHesse(bool flag);                                             // Set save hessian matrix as multi-Gaussian
  void setFitHesse(bool flag) { _fitHesse = flag; }                         // Fit hessian matrix to optimize initial values

  // Fitting
  bool checkModel(const RooStats::ModelConfig &model, bool throwOnFail = false); // Check integrity of fit model
  int profileToData(ModelConfig *mc, RooAbsData *data);                          // Fit to data, core algorithm
  void minimize(RooAbsReal *nll, ModelConfig *mc);                               // Iminimization
  int status() { return _status; }                                               // Get fit status
  void setBinnedLHAttr(ModelConfig *mc);                                         // Set binned attribute (accelerate fitting of HistFactory-based model)
  RooAbsReal *createNLL(ModelConfig *mc, RooAbsData *data);                      // Construct NLL
  void createMultiGaus(ModelConfig *mc, RooFitResult *res);                      // Create multi-Gaussian from the hesse matrix
  double minimizeHessePdf(RooAbsPdf *pdf);                                       // Minimize the hesse pdf
};

#endif

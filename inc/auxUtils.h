#ifndef AUXUTILS_HEADER
#define AUXUTILS_HEADER

#include "RooStatsHead.h"
#include "RooFitHead.h"
#include "CommonHead.h"

using namespace std;
using namespace RooFit;
using namespace RooStats;

class auxUtils {
public:
  static std::vector<std::string> Tokenize(const std::string& str, const std::string& delimiters);
  static vector<TString> splitString(const TString& theOpt, const char separator );
  static TXMLAttr *findAttribute(TXMLNode* rootNode, TString attributeKey);
  static void removeWhiteSpace(TString& item);
  static TString getAttributeValue( TXMLNode* rootNode, TString attributeKey, bool allowEmpty=false, TString defaultStr="");
  static void printTitle(TString titleText, TString separator, int width=10);
  static void alertAndAbort(TString msg);

  static string OKGREEN;
  static string FAIL;
  static string ENDC;
};

#endif


/* Add your custom classes in this file */
/* Remember also to modify inc/rooCommon.h */
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

/* Custom classes (other than those in RooFitExtensions) */
// #pragma link C++ class HggMG5aMCNLOLineShapePdf+;

#endif

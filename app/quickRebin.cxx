#include "CommonHead.h"
#include "RooFitHead.h" 
#include "RooStatsHead.h"

#include <boost/program_options.hpp>

#include "auxUtils.h"
#include "utils.h"

std::string _outputFile = "";
std::string _inputFile = "";
std::string _dataName = "toyData";
std::string _wsName = "combWS";
std::string _mcName = "ModelConfig";

int _rebin = 500;

int main( int argc, char** argv )
{
  namespace po = boost::program_options;
  po::options_description desc( "quickFit options" );
  desc.add_options()
    // IO Options 
    ( "inputFile,f",   po::value<std::string>(&_inputFile)->required(),  "Specify the input TFile (REQUIRED)" )

    ( "outputFile,o",  po::value<std::string>(&_outputFile)->required(), "Save fit results to output TFile (REQUIRED)" )

    ( "dataName,d",    po::value<std::string>(&_dataName)->default_value(_dataName),   
                         "Name of the dataset" )
    ( "wsName,w",      po::value<std::string>(&_wsName)->default_value(_wsName),
                         "Name ofk the workspace" )
    ( "mcName,m",      po::value<std::string>(&_mcName)->default_value(_mcName), 
                         "Name of the model config" )
    ( "rebin,r",      po::value<int>(&_rebin)->default_value(_rebin), 
                         "Name of the model config" )
    ( "help,h",          "Print help message")
    ;

  po::variables_map vm;
  try
  {
    po::store( po::command_line_parser( argc, argv ).options( desc ).run(), vm );
    po::notify( vm );
  }
  catch ( std::exception& ex )
  {
    std::cerr << "Invalid options: " << ex.what() << std::endl;
    std::cout << "Invalid options: " << ex.what() << std::endl;
    std::cout << "Use manager --help to get a list of all the allowed options"  << std::endl;
    return 999;
  }
  catch ( ... )
  {
    std::cerr << "Unidentified error parsing options." << std::endl;
    return 1000;
  }

  // if help, print help
  if ( !vm.count("inputFile") || vm.count( "help" ) )
  {
    std::cout << "Usage: manager [options]\n";
    std::cout << desc;
    return 0;
  }

  RooMsgService::instance().getStream(1).removeTopic(RooFit::NumIntegration) ;
  RooMsgService::instance().getStream(1).removeTopic(RooFit::Fitting) ;
  RooMsgService::instance().getStream(1).removeTopic(RooFit::Minimization) ;
  RooMsgService::instance().getStream(1).removeTopic(RooFit::InputArguments) ;
  RooMsgService::instance().getStream(1).removeTopic(RooFit::Eval) ;
  RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR);
  
  // Get workspace, model, and data from file
  TFile *tf = TFile::Open( (TString) _inputFile );
  if (not tf->IsOpen()) {
    std::cout << "Error: TFile \'" << _inputFile << "\' was not found." << endl;
    return 1;
  }

  RooWorkspace *ws = (RooWorkspace*)tf->Get( (TString) _wsName );
  if (ws == nullptr) {
    std::cout << "Error: Workspace \'" << _wsName << "\' does not exist in the TFile." << endl;
    return 1;
  }
  
  RooStats::ModelConfig *mc = (RooStats::ModelConfig*)ws->obj( (TString) _mcName );
  if (mc == nullptr) {
    std::cout << "Error: ModelConfig \'" << _mcName << "\' does not exist in workspace." << endl;
    return 1;
  }
  
  RooAbsData *data = ws->data( (TString) _dataName );
  if (data == nullptr) {
    std::cout << "Error: Dataset \'" << _dataName << "\' does not exist in workspace." << endl;
    return 1;
  }
  
  TStopwatch timer;
  cout << endl << "Starting rebinning data "<< _dataName <<" with number of bins "<<_rebin<<"..." << endl;
  
  RooRealVar* weightVar = new RooRealVar( "_weight_", "", 1. );
  std::map<std::string, RooDataSet*> m_dataMap;
  RooArgSet m_obsAndWgt;

  std::unique_ptr<TIterator> iter(mc->GetObservables()->createIterator());
  for ( RooRealVar* v = (RooRealVar*)iter->Next(); v!=0; v = (RooRealVar*)iter->Next() ) {
    if ( std::string(v->ClassName())!=std::string("RooRealVar") ) { continue; }
    m_obsAndWgt.add(*v);
  }

  m_obsAndWgt.add( *weightVar );

  RooSimultaneous* m_pdf = dynamic_cast<RooSimultaneous*>(mc->GetPdf());
  RooCategory*  m_cat = (RooCategory*)(&m_pdf->indexCat());
  TList* m_dataList = data->split( *m_cat, true );
  int numChannels = m_cat->numBins(0);
  
  for ( int i= 0; i < numChannels; i++ ) {
    m_cat->setBin(i);
    RooAbsPdf* pdfi = m_pdf->getPdf(m_cat->getLabel());
    RooDataSet* datai = ( RooDataSet* )( m_dataList->FindObject(m_cat->getLabel()) );
    RooArgList obs (*pdfi->getObservables(*datai));
    RooRealVar* obsVar = (RooRealVar*)obs.at(0);
    int numBins=obsVar->getBins();
    int numEntries = datai->numEntries();
    int sumEntries = datai->sumEntries();

    bool isBinned = (numEntries!=sumEntries);
    isBinned += (numEntries<_rebin);
    if(isBinned){
      TString type = m_cat->getLabel();
      m_dataMap[type.Data()] = datai;
    }
    else{
      std::string dataiName = datai->GetName();
      std::cout << "\tRebin " << dataiName
		<< ", numEntries: "<<numEntries
		<< ", sumEntries: "<<sumEntries
		<< ", numBins: "<<numBins
		<< std::endl;
      datai->SetName((dataiName+"_old").c_str());
      
      RooArgSet obsPlusW;
      obsPlusW.add( obs );
      obsPlusW.add( *weightVar );
	
      TH1* hist = datai->createHistogram((dataiName+"_hist").c_str(), *obsVar, RooFit::Binning(_rebin, obsVar->getMin(), obsVar->getMax()));
      RooBinning rebin(_rebin, obsVar->getMin(), obsVar->getMax());
      obsVar->setBinning(rebin);
      
      RooDataSet* dataiNew = new RooDataSet( dataiName.c_str(), "", obsPlusW, weightVar->GetName() );
	
      for ( int i = 1, n = hist->GetNbinsX(); i <= n; ++i ) {
	// std::cout << "bin content: " << hist->GetBinContent(i) << std::endl;
	obsVar->setVal( hist->GetXaxis()->GetBinCenter( i ) );
	dataiNew->add( obs, hist->GetBinContent( i ) );
      }
	
      TString type = m_cat->getLabel();
      m_dataMap[type.Data()] = dataiNew;
    }
  }
  
  unique_ptr<RooDataSet> newData(new RooDataSet((_dataName+"binned").c_str(),_dataName.c_str(),
						m_obsAndWgt,
						RooFit::Index( *m_cat ),
						RooFit::Import( m_dataMap ) ,
						RooFit::WeightVar( *weightVar ) /* actually just pass a name */
						));
  
  std::cout << "numEntries: " << newData->numEntries() << std::endl;
  std::cout << "sumEntries: " << newData->sumEntries() << std::endl;
  ws->import(*newData.get());
  timer.Stop();
  double t_cpu_ = timer.CpuTime()/60.;
  double t_real_ = timer.RealTime()/60.;
  printf("\nAll operations done in %.2f min (cpu), %.2f min (real)\n", t_cpu_, t_real_);
  // Print summary
  ws->writeToFile(_outputFile.c_str());
  cout << endl << "  Workspace with rebinned dataset (" << _dataName << ") saved in output file "
       << _outputFile << endl;
  return 0;
}


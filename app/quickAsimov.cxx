#include "CommonHead.h"
#include "RooFitHead.h" 
#include "RooStatsHead.h"

#include <boost/program_options.hpp>

#include "fitTool.h"
#include "auxUtils.h"
#include "asimovTool.h"

std::string _inputFile = "";
std::string _minAlgo  = "Minuit2";
std::string _dataName = "combData";
std::string _wsName = "combWS";
std::string _mcName = "ModelConfig";

bool _checkWS = false;
bool _nllOffset = true;
bool _fixStarCache = false;
float _minTolerance = 0.001;
int _minStrategy = 1;
int _optConst = 2;
int _printLevel = 2;
int _nCPU = 1;
bool _useBatch = false;
float _samplingRelTol = -1.;

string OKGREEN = "\033[92m";
string FAIL = "\033[91m";
string ENDC = "\033[0m";

int main( int argc, char** argv )
{
  namespace po = boost::program_options;
  po::options_description desc( "quickFit options" );
  desc.add_options()
    // IO Options 
    ( "xml,x",   po::value<std::string>(&_inputFile),  "Specify the input XML file (REQUIRED)" )

    ( "dataName,d",    po::value<std::string>(&_dataName)->default_value(_dataName),   
                         "Name of the dataset" )
    ( "wsName,w",      po::value<std::string>(&_wsName)->default_value(_wsName),
                         "Name of the workspace" )
    ( "mcName,m",      po::value<std::string>(&_mcName)->default_value(_mcName), 
                         "Name of the model config" )
    ( "nllOffset",     po::value<bool>(&_nllOffset)->default_value(_nllOffset),         
                         "Set NLL offset" )
    ( "numCPU",      po::value<int>(&_nCPU)->default_value(_nCPU),
                         "Set number of CPUs for fit" )
    ( "minStrat",      po::value<int>(&_minStrategy)->default_value(_minStrategy),
                         "Set minimizer strategy" )
    ( "optConst",      po::value<int>(&_optConst)->default_value(_optConst),
                         "Set optimize constant" ) ( "printLevel",    po::value<int>(&_printLevel)->default_value(_printLevel),
                         "Set minimizer print level" )
    ( "minTolerance",  po::value<float>(&_minTolerance)->default_value(_minTolerance),
                         "Set minimizer tolerance" )
    // Other
    ( "help,h",          "Print help message")

    ( "checkWS",       po::value<bool>(&_checkWS)->default_value(_checkWS),
                         "Perform sanity checks on workspace before fit." )
    ( "fixStarCache",  po::value<bool>(&_fixStarCache)->default_value(_fixStarCache),
                         "Fix cache in RooStarMomentMorph." )
    #if ROOT_VERSION_CODE >= ROOT_VERSION(6,24,3)
    ( "useBatch",     po::value<bool>(&_useBatch)->default_value(_useBatch),
                         "If using vectorize computation on CPU." )
    ( "samplingRelTol",  po::value<float>(&_samplingRelTol)->default_value(_samplingRelTol),
                         "Set relative tolerance for sampling of unbinned PDFs in binned fits." )
    #endif
    ;

  po::variables_map vm;
  try
  {
    po::store( po::command_line_parser( argc, argv ).options( desc ).run(), vm );
    po::notify( vm );
  }
  catch ( std::exception& ex )
  {
    std::cerr << "Invalid options: " << ex.what() << std::endl;
    std::cout << "Invalid options: " << ex.what() << std::endl;
    std::cout << "Use "<<argv[0]<<" --help to get a list of all the allowed options"  << std::endl;
    return 999;
  }
  catch ( ... )
  {
    std::cerr << "Unidentified error parsing options." << std::endl;
    return 1000;
  }

  // if help, print help
  if ( !vm.count("xml") || vm.count( "help" ) )
  {
    std::cout << "Usage: "<<argv[0]<<" [options]\n";
    std::cout << desc;
    return 0;
  }

  RooMsgService::instance().getStream(1).removeTopic(RooFit::NumIntegration) ;
  RooMsgService::instance().getStream(1).removeTopic(RooFit::Fitting) ;
  RooMsgService::instance().getStream(1).removeTopic(RooFit::Minimization) ;
  RooMsgService::instance().getStream(1).removeTopic(RooFit::InputArguments) ;
  RooMsgService::instance().getStream(1).removeTopic(RooFit::Eval) ;
  RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR);

  // Prepare XML parser
  TDOMParser xmlparser;
  xmlparser.ParseFile(_inputFile.c_str());
  TXMLDocument* xmldoc=xmlparser.GetXMLDocument();
  TXMLNode* rootNode=xmldoc->GetRootNode();
  TXMLNode* node=rootNode->GetChildren();

  TString inputFileName=auxUtils::getAttributeValue(rootNode, "InputFile");
  TString outputFileName=auxUtils::getAttributeValue(rootNode, "OutputFile");
  TString POIStr=auxUtils::getAttributeValue(rootNode, "POI", true, "");
  
  // Get workspace, model, and data from file
  TFile *tf = TFile::Open( inputFileName );
  if (not tf->IsOpen()) {
    std::cout << "Error: TFile \'" << _inputFile << "\' was not found." << endl;
    return 1;
  }

  RooWorkspace *ws = (RooWorkspace*)tf->Get( (TString) _wsName );
  if (ws == nullptr) {
    std::cout << "Error: Workspace \'" << _wsName << "\' does not exist in the TFile." << endl;
    return 1;
  }
  
  RooStats::ModelConfig *mc = (RooStats::ModelConfig*)ws->obj( (TString) _mcName );
  if (mc == nullptr) {
    std::cout << "Error: ModelConfig \'" << _mcName << "\' does not exist in workspace." << endl;
    return 1;
  }

  // Give you a change to re-define POI list
  if(POIStr!=""){
    vector<TString> POIList=auxUtils::splitString(POIStr,',');
    RooArgSet POISet;
    for(auto POIName : POIList){
      RooRealVar *poi=ws->var(POIName);
      if(!poi) auxUtils::alertAndAbort("POI "+POIName+" does not exist.");
      POISet.add(*poi);
    }
    mc->SetParametersOfInterest(POISet);
  }

  RooStats::SetAllConstant( *mc->GetGlobalObservables(), true );
  RooStats::SetAllConstant( *mc->GetNuisanceParameters(), false );
  RooStats::SetAllConstant( *mc->GetParametersOfInterest(), true );

  cout<<"Current values of POIs:"<<endl;
  mc->GetParametersOfInterest()->Print("v");
  
  RooAbsData *data = ws->data( (TString) _dataName );
  if (data == nullptr) {
    std::cout << "Error: Dataset \'" << _dataName << "\' does not exist in workspace." << endl;
    return 1;
  }

  // Setup fitter. No output
  fitTool *fitter = new fitTool();
  fitter->setMinAlgo( (TString) _minAlgo );
  fitter->setNLLOffset( _nllOffset );
  fitter->setTolerance( _minTolerance );
  fitter->setStrategy( _minStrategy );
  fitter->setOptConst( _optConst );
  fitter->setPrintLevel( _printLevel );
  fitter->setNCPU( _nCPU );
  fitter->setFixStarCache( _fixStarCache );
  #if ROOT_VERSION_CODE >= ROOT_VERSION(6,24,3)
  fitter->setBatchMode( _useBatch );
  fitter->setSamplingRelTol( _samplingRelTol );
  #endif
  
  // Sanity checks on model 
  if (_checkWS) {
    cout << "Performing sanity checks on model..." << endl;
    bool validModel = fitter->checkModel( *mc, true );
    cout << "Sanity checks on the model: " << (validModel ? "OK" : "FAIL") << endl;
  }

  // Setup Asimov handler
  unique_ptr<asimovTool> asimovHandler(new asimovTool(fitter));
  while ( node != 0 ){
    TString nodeName=node->GetNodeName();
    if(nodeName=="Action"){
      asimovHandler->addEntry(node);
    }
    node=node->GetNextNode();
  }

  // Start creating Asimov
  TStopwatch timer;
  cout << endl << "Starting generating Asimov..." << endl;
  asimovHandler->generateAsimov(mc, _dataName);
    
  timer.Stop();
  double t_cpu_ = timer.CpuTime()/60.;
  double t_real_ = timer.RealTime()/60.;
  printf("\nAll operations done in %.2f min (cpu), %.2f min (real)\n", t_cpu_, t_real_);

  ws->importClassCode();
  ws->writeToFile(outputFileName);

  cout<<"Workspace saved at "<<outputFileName<<endl;
  return 0;
}

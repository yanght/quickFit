#include "CommonHead.h"
#include "RooFitHead.h" 
#include "RooStatsHead.h"

#include <boost/program_options.hpp>

#include "limitTool.h"
#include "auxUtils.h"

#include "spdlog/spdlog.h"

std::string _outputFile = "";
std::string _inputFile = "";
std::string _minAlgo  = "Minuit2";
std::string _dataName = "combData";
std::string _wsName = "combWS";
std::string _mcName = "ModelConfig";
std::string _snapshot = "";

std::string _poiStr = "";
std::string _fixNPStr = "";

std::string _externalConstraint = "";
std::string _asimovDataName = "";
  
bool _saveWS = false;
bool _saveErrors = false;
bool _checkWS = false;
bool _nllOffset = true;
bool _fixStarCache = false;
float _minTolerance = 0.001;
int _minStrategy = 1;
int _optConst = 2;
int _printLevel = -1;
int _nCPU = 1;
double _initial_guess = 1;
bool _saveFitResult = false;
bool _betterBands = true;
bool _doBlind = false;
bool _GKIntegrator = false;
bool _useBatch = false;
float _samplingRelTol = -1.;
int _testStatMode = 1; // 0: q_mu; 1: q_mu_tilde, with unconditional fit allow going negative; 2: q_mu_tilde, with unconditional fit constrained to be positive
bool _doExp = true;
bool _doObs = true;

int main( int argc, char** argv )
{
  spdlog::set_pattern("[%Y-%m-%d %H:%M:%S] [thread %t] [%^%l%$] \t %v");
  namespace po = boost::program_options;
  po::options_description desc( "quickLimit options" );
  desc.add_options()
    // IO Options 
    ( "inputFile,f",   po::value<std::string>(&_inputFile),  "Specify the input TFile (REQUIRED)" )

    ( "outputFile,o",  po::value<std::string>(&_outputFile), "Save fit results to output TFile (REQUIRED)" )

    ( "dataName,d",    po::value<std::string>(&_dataName)->default_value(_dataName),   
      "Name of the dataset" )
    ( "wsName,w",      po::value<std::string>(&_wsName)->default_value(_wsName),
      "Name of the workspace" )
    ( "mcName,m",      po::value<std::string>(&_mcName)->default_value(_mcName), 
      "Name of the model config" )
    ( "snapshot,s",    po::value<std::string>(&_snapshot)->default_value(_snapshot), 
      "Load snapshot from workspace" )
    ( "asimovDataName,a",      po::value<std::string>(&_asimovDataName)->default_value(_asimovDataName), 
      "Name of backgrond-only Asimov dataset to be used in limit setting. PLEASE MAKE SURE SNAPSHOTS 'conditionalNuis_0' AND 'conditionalGlobs_0' ARE SAVED IN WORKSPACE!!!" )
    // Model Options
    ( "poi,p",         po::value<std::string>(&_poiStr),     "Specify POIs to be used in fit" )

    ( "fixNP,n",       po::value<std::string>(&_fixNPStr),   "Specify NPs to be used in fit" )

    // Fit Options
    ( "nllOffset",     po::value<bool>(&_nllOffset)->default_value(_nllOffset),         
      "Set NLL offset" )
    ( "numCPU",      po::value<int>(&_nCPU)->default_value(_nCPU),
      "Set number of CPUs for fit" )
    ( "minStrat",      po::value<int>(&_minStrategy)->default_value(_minStrategy),
      "Set minimizer strategy" )
    ( "optConst",      po::value<int>(&_optConst)->default_value(_optConst),
      "Set optimize constant" ) ( "printLevel",    po::value<int>(&_printLevel)->default_value(_printLevel),
				  "Set minimizer print level" )
    ( "minTolerance",  po::value<float>(&_minTolerance)->default_value(_minTolerance),
      "Set minimizer tolerance" )
    ( "saveWS",        po::value<bool>(&_saveWS)->default_value(_saveWS),
      "Save postfit workspace to the output file" )
    ( "externalConstraint",        po::value<std::string>(&_externalConstraint)->default_value(_externalConstraint),
      "Name of external constraint pdf to be included in the fit. If there are multiple of them, use comma to separate." )
    ( "saveErrors",    po::value<bool>(&_saveErrors)->default_value(_saveErrors),
      "Save errors in the TTree" )
    
    // Other
    ( "help,h",          "Print help message")

    ( "checkWS",       po::value<bool>(&_checkWS)->default_value(_checkWS),
      "Perform sanity checks on workspace before fit." )
    ( "fixStarCache",  po::value<bool>(&_fixStarCache)->default_value(_fixStarCache),
      "Fix cache in RooStarMomentMorph." )
    ( "initialGuess",  po::value<double>(&_initial_guess)->default_value(_initial_guess),
      "Initial guess of expected limit." )
    ( "savefitresult",         po::value<bool>(&_saveFitResult)->default_value(_saveFitResult),
      "Save fit result" )
    ( "betterBands",         po::value<bool>(&_betterBands)->default_value(_betterBands),
      "Calculate corrected limit band." )
    ( "doBlind",         po::value<bool>(&_doBlind)->default_value(_doBlind),
      "Run blinded (expect only) limit." )
    ( "GKIntegrator",  po::value<bool>(&_GKIntegrator)->default_value(_GKIntegrator),
      "Use Gauss Kronrod Integrator." )
    ( "doExp",  po::value<bool>(&_doExp)->default_value(_doExp),
      "Derive expected limit." )
    ( "doObs",  po::value<bool>(&_doObs)->default_value(_doObs),
      "Derive observed limit." )
    ( "testStatMode",  po::value<int>(&_testStatMode)->default_value(_testStatMode),
      "Test statistic mode. 0: q_mu; 1: q_mu_tilde, with unconditional fit allow going negative; 2: q_mu_tilde, with unconditional fit constrained to be positive" )      
    #if ROOT_VERSION_CODE >= ROOT_VERSION(6,24,3)
    ( "useBatch",     po::value<bool>(&_useBatch)->default_value(_useBatch),
                         "If using vectorize computation on CPU." )
    ( "samplingRelTol",  po::value<float>(&_samplingRelTol)->default_value(_samplingRelTol),
                         "Set relative tolerance for sampling of unbinned PDFs in binned fits." )
    #endif
    ;

  po::variables_map vm;
  try
  {
    po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);
    po::notify(vm);
  }
  catch (std::exception &ex)
  {
    spdlog::error("Invalid options: {}", ex.what());
    spdlog::error("Use '{} --help' to get a list of all the allowed options", argv[0]);
    return 999;
  }
  catch (...)
  {
    spdlog::error("Unidentified error parsing options...");
    return 1000;
  }

  // if help, print help
  if (!vm.count("inputFile") || !vm.count("outputFile") || vm.count("help"))
  {
    spdlog::info("Usage: {} [options]", argv[0]);
    std::cout << desc;
    return 0;
  }

  RooMsgService::instance().getStream(1).removeTopic(RooFit::NumIntegration) ;
  RooMsgService::instance().getStream(1).removeTopic(RooFit::Fitting) ;
  RooMsgService::instance().getStream(1).removeTopic(RooFit::Minimization) ;
  RooMsgService::instance().getStream(1).removeTopic(RooFit::InputArguments) ;
  RooMsgService::instance().getStream(1).removeTopic(RooFit::Eval) ;
  RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR);

  // Get workspace, model, and data from file
  TFile *tf = TFile::Open(_inputFile.c_str());
  if (not tf->IsOpen())
  {
    spdlog::error("TFile '{}' was not found...", _inputFile.c_str());
    return 1;
  }

  RooWorkspace *ws = (RooWorkspace *)tf->Get(_wsName.c_str());
  if (ws == nullptr)
  {
    spdlog::error("Workspace '{}' does not exist in the TFile...", _wsName.c_str());
    return 1;
  }

  RooStats::ModelConfig *mc = (RooStats::ModelConfig *)ws->obj(_mcName.c_str());
  if (mc == nullptr)
  {
    spdlog::error("ModelConfig '{}' does not exist in workspace...", _mcName.c_str());
    return 1;
  }

  RooDataSet *data = (RooDataSet *)ws->data(_dataName.c_str());
  if (data == nullptr)
  {
    spdlog::error("Dataset '{}' does not exist in workspace.", _dataName.c_str());
    return 1;
  }

  utils::loadSnapshot(ws, _snapshot);
  
  // save a snapshot of everything as is
  RooArgSet everything;
  utils::collectEverything(mc, &everything);
  ws->saveSnapshot( "original", everything);

  // Prepare model as expected
  RooStats::SetAllConstant( *mc->GetGlobalObservables(), true );
  RooStats::SetAllConstant( *mc->GetNuisanceParameters(), false );
  RooStats::SetAllConstant( *mc->GetParametersOfInterest(), true );

  // Fix nuisance narameters
  if (vm.count("fixNP"))
    utils::fixNP(mc, _fixNPStr);

  // Set external PDF (should be save in the same workspace)
  RooArgSet externalConstrSet;
  if (vm.count("externalConstraint"))
    utils::setExtConstr(ws, _externalConstraint, &externalConstrSet);

  // Prepare parameters of interest
  RooArgSet fitPOIs;
  RooRealVar *firstPOI=utils::setupPOI(mc, _poiStr, &fitPOIs, true);
  
  spdlog::info("Summary of POI setup in the likelihood model:");
  fitPOIs.Print("v");

  // Setup fitter. No output
  fitTool *fitter = new fitTool();
  fitter->setMinAlgo( (TString) _minAlgo );
  fitter->setNLLOffset( _nllOffset );
  fitter->setTolerance( _minTolerance );
  fitter->setStrategy( _minStrategy );
  fitter->setOptConst( _optConst );
  fitter->setPrintLevel( _printLevel );
  fitter->setNCPU( _nCPU );
  fitter->setFixStarCache( _fixStarCache );
  fitter->saveFitResult( _saveFitResult);
  fitter->setGKIntegrator( _GKIntegrator );
  fitter->setBinnedLHAttr(mc);
  #if ROOT_VERSION_CODE >= ROOT_VERSION(6,24,3)
  fitter->setBatchMode( _useBatch );
  fitter->setSamplingRelTol( _samplingRelTol );
  #endif
  
  // Sanity checks on model 
  if (_checkWS) {
    spdlog::info("Performing sanity checks on model...");
    bool validModel = fitter->checkModel( *mc, true );
    spdlog::info("Sanity checks on the model: {}", (validModel ? "OK" : "FAIL"));
  }

  // Set fit options
  limitTool *limitCalculator = new limitTool(ws, mc, data, fitter, firstPOI, _initial_guess);
  limitCalculator->setBetterBands(_betterBands);
  limitCalculator->setTestStatisticMode(_testStatMode);
  limitCalculator->setDoExp(_doExp);
  limitCalculator->setDoObs(_doObs);
  limitCalculator->setBlind(_doBlind);
  
  // Fitting 
  TStopwatch timer;
  spdlog::info("Starting limit setting...");
  if (_asimovDataName != "" && (!ws->loadSnapshot("conditionalNuis_0") || !ws->loadSnapshot("conditionalGlobs_0")))
  {
    spdlog::error("When using user-defined background-only Asimov data, snapshots 'conditionalNuis_0' and 'conditionalGlobs_0' must be provied...");
    return 1;
  }
  limitCalculator->runAsymptoticsCLs(_outputFile, _asimovDataName.c_str()); // Perform fit
  timer.Stop();
  double t_cpu_ = timer.CpuTime()/60.;
  double t_real_ = timer.RealTime()/60.;
  spdlog::info("All fits done in {} min (cpu), {} min (real)", t_cpu_, t_real_);
  tf->Close();
  return 0;
}

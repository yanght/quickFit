#include "CommonHead.h"
#include "RooFitHead.h" 
#include "RooStatsHead.h"

#include <boost/program_options.hpp>

#include "fitTool.h"
#include "auxUtils.h"

#include "spdlog/spdlog.h"

std::string _ssname = "ucmles";
std::string _outputFile = "";
std::string _inputFile = "";
std::string _minAlgo  = "Minuit2";
std::string _dataName = "combData";
std::string _wsName = "combWS";
std::string _mcName = "ModelConfig";
std::string _snapshot = "";

std::string _poiStr = "";
std::string _fixNPStr = "";
std::string _fixAttStr = "";

std::string _externalConstraint = "";
std::string _rangeName = "";

bool _saveWS = false;
bool _saveErrors = false;
bool _checkWS = false;
bool _useHESSE = false;
int _useMINOS = 0;
bool _useSIMPLEX = false;
bool _nllOffset = true;
bool _fixStarCache = false;
float _minTolerance = 0.001;
float _errorLevel = -1;
int _minStrategy = 1;
int _optConst = 2;
int _printLevel = 2;
int _nCPU = 1;
bool _saveFitResult = true;
bool _GKIntegrator = false;
int _storageLevel = 0;
bool _saveNP = false;
bool _setSumW2 = false;
bool _useBatch = false;
float _samplingRelTol = -1.;
bool _saveHesse = false;
bool _fitHesse = false;

int main( int argc, char** argv )
{
  spdlog::set_pattern("[%Y-%m-%d %H:%M:%S] [thread %t] [%^%l%$] \t %v");
  namespace po = boost::program_options;
  po::options_description desc( "quickFit options" );
  desc.add_options()
    // IO Options 
    ( "inputFile,f",   po::value<std::string>(&_inputFile),  "Specify the input TFile (REQUIRED)" )

    ( "outputFile,o",  po::value<std::string>(&_outputFile), "Save fit results to output TFile" )

    ( "dataName,d",    po::value<std::string>(&_dataName)->default_value(_dataName),   
                         "Name of the dataset" )
    ( "wsName,w",      po::value<std::string>(&_wsName)->default_value(_wsName),
                         "Name of the workspace" )
    ( "mcName,m",      po::value<std::string>(&_mcName)->default_value(_mcName), 
                         "Name of the model config" )
    ( "snapshot,s",    po::value<std::string>(&_snapshot)->default_value(_snapshot), 
                         "Load snapshot from workspace" )
    ( "ssname,k",      po::value<std::string>(&_ssname)->default_value(_ssname), 
                         "Name of snapshot to save to output workspace" )
    // Model Options
    ( "poi,p",         po::value<std::string>(&_poiStr),     "Specify POIs to be used in fit" )

    ( "fixNP,n",       po::value<std::string>(&_fixNPStr),   "Specify NPs to be fixed in fit" )
    ( "fixAtt",       po::value<std::string>(&_fixAttStr),   "Specify attribute to be used to fix NPs" )

    // Fit Options
    ( "simplex",       po::value<bool>(&_useSIMPLEX)->default_value(_useSIMPLEX),
                         "Estimate central values with SIMPLEX" )
    ( "hesse",         po::value<bool>(&_useHESSE)->default_value(_useHESSE),
                         "Estimate errors with HESSE after fit" )
    ( "minos",         po::value<int>(&_useMINOS)->default_value(_useMINOS),
                         "Get asymmetric errors with MINOS fit. Choose from 0 (no minos fit), 1 (only POIs), 2 (only NPs) 3 (both POIs and NPs)" )
    ( "savefitresult",         po::value<bool>(&_saveFitResult)->default_value(_saveFitResult),
                         "Save fit result" )
    ( "nllOffset",     po::value<bool>(&_nllOffset)->default_value(_nllOffset),         
                         "Set NLL offset" )
    ( "numCPU",      po::value<int>(&_nCPU)->default_value(_nCPU),
                         "Set number of CPUs for fit" )
    ( "minStrat",      po::value<int>(&_minStrategy)->default_value(_minStrategy),
                         "Set minimizer strategy" )
    ( "optConst",      po::value<int>(&_optConst)->default_value(_optConst),
                         "Set optimize constant" ) ( "printLevel", po::value<int>(&_printLevel)->default_value(_printLevel),
                         "Set minimizer print level" )
    ( "minTolerance",  po::value<float>(&_minTolerance)->default_value(_minTolerance),
                         "Set minimizer tolerance" )
    ( "saveWS",        po::value<bool>(&_saveWS)->default_value(_saveWS),
                         "Save postfit workspace to the output file" )
    ( "externalConstraint",        po::value<std::string>(&_externalConstraint)->default_value(_externalConstraint),
                         "Name of external constraint pdf to be included in the fit. If there are multiple of them, use comma to separate." )
    ( "saveErrors",    po::value<bool>(&_saveErrors)->default_value(_saveErrors),
                         "Save errors in the TTree" )
    // Other
    ( "help,h",          "Print help message")

    ( "checkWS",       po::value<bool>(&_checkWS)->default_value(_checkWS),
                         "Perform sanity checks on workspace before fit." )
    ( "fixStarCache",  po::value<bool>(&_fixStarCache)->default_value(_fixStarCache),
                         "Fix cache in RooStarMomentMorph." )
    ( "GKIntegrator",  po::value<bool>(&_GKIntegrator)->default_value(_GKIntegrator),
                         "Use Gauss Kronrod Integrator." )
    ( "storageLevel",  po::value<int>(&_storageLevel)->default_value(_storageLevel),
      "Set storage level." )
    ( "saveNP",  po::value<bool>(&_saveNP)->default_value(_saveNP),
                         "Save nuisance parameter values in fit result." )
    ( "setSumW2",  po::value<bool>(&_setSumW2)->default_value(_setSumW2),
                         "Set SumW2 error" )
    ( "errorLevel",  po::value<float>(&_errorLevel)->default_value(_errorLevel),
                         "Set error level." )
    ( "range,r",    po::value<std::string>(&_rangeName)->default_value(_rangeName),
                         "Set fit range name" )
    ( "saveHesse",    po::value<bool>(&_saveHesse)->default_value(_saveHesse),
                         "Save Hesse pdf" )
    ( "fitHesse",    po::value<bool>(&_fitHesse)->default_value(_fitHesse),
                         "Fit Hesse pdf to optimize variable values" )
    #if ROOT_VERSION_CODE >= ROOT_VERSION(6,24,3)
    ( "useBatch",     po::value<bool>(&_useBatch)->default_value(_useBatch),
                         "If using vectorize computation on CPU." )
    ( "samplingRelTol",  po::value<float>(&_samplingRelTol)->default_value(_samplingRelTol),
                         "Set relative tolerance for sampling of unbinned PDFs in binned fits." )
    #endif
    ;

  po::variables_map vm;
  try
  {
    po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);
    po::notify(vm);
  }
  catch (std::exception &ex)
  {
    spdlog::error("Invalid options: {}", ex.what());
    spdlog::error("Use '{} --help' to get a list of all the allowed options", argv[0]);
    return 999;
  }
  catch (...)
  {
    spdlog::error("Unidentified error parsing options...");
    std::cout << desc;
    return 1000;
  }

  // if help, print help
  if (!vm.count("inputFile") || vm.count("help"))
  {
    spdlog::info("Usage: {} [options]", argv[0]);
    std::cout << desc;
    return 0;
  }

  RooMsgService::instance().getStream(1).removeTopic(RooFit::NumIntegration);
  RooMsgService::instance().getStream(1).removeTopic(RooFit::Fitting);
  RooMsgService::instance().getStream(1).removeTopic(RooFit::Minimization);
  RooMsgService::instance().getStream(1).removeTopic(RooFit::InputArguments);
  RooMsgService::instance().getStream(1).removeTopic(RooFit::Eval);
  RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR);

  // Get workspace, model, and data from file
  TFile *tf = TFile::Open(_inputFile.c_str());
  if (not tf->IsOpen())
  {
    spdlog::error("TFile '{}' was not found...", _inputFile.c_str());
    return 1;
  }

  RooWorkspace *ws = (RooWorkspace *)tf->Get(_wsName.c_str());
  if (ws == nullptr)
  {
    spdlog::error("Workspace '{}' does not exist in the TFile...", _wsName.c_str());
    return 1;
  }

  RooStats::ModelConfig *mc = (RooStats::ModelConfig *)ws->obj(_mcName.c_str());
  if (mc == nullptr)
  {
    spdlog::error("ModelConfig '{}' does not exist in workspace...", _mcName.c_str());
    return 1;
  }

  RooDataSet *data = (RooDataSet *)ws->data(_dataName.c_str());
  if (data == nullptr)
  {
    spdlog::error("Dataset '{}' does not exist in workspace.", _dataName.c_str());
    return 1;
  }

  // save a snapshot of everything as is
  RooArgSet everything;
  utils::collectEverything(mc, &everything);
  ws->saveSnapshot("original", everything);

  // Prepare model as expected
  RooStats::SetAllConstant(*mc->GetGlobalObservables(), true);    // Fix all global observables
  RooStats::SetAllConstant(*mc->GetNuisanceParameters(), false);  // Float all nuisance parameters
  RooStats::SetAllConstant(*mc->GetParametersOfInterest(), true); // Fix all the POIs

  utils::loadSnapshot(ws, _snapshot); // Load snapshot (which may overwrite the fix/float status of NP/GO/POI above, so be careful and make sure you understand the content of the snapshot)

  // Fix nuisance narameters
  if (vm.count("fixNP"))
    utils::fixNP(mc, _fixNPStr);

  if (vm.count("fixAtt"))
    utils::fixAtt(mc, _fixAttStr);

  // Set external PDF (should be save in the same workspace)
  RooArgSet externalConstrSet;
  if (vm.count("externalConstraint"))
    utils::setExtConstr(ws, _externalConstraint, &externalConstrSet);

  RooArgSet fitPOIs;
  // Prepare parameters of interest
  utils::setupPOI(mc, _poiStr, &fitPOIs);

  spdlog::info("Summary of POI setup in the likelihood model");
  fitPOIs.Print("v");

  // Set fit options
  fitTool *fitter = new fitTool();
  fitter->setMinAlgo((TString)_minAlgo);
  fitter->useHESSE(_useHESSE);
  fitter->useMINOS(_useMINOS);
  fitter->useSIMPLEX(_useSIMPLEX);
  fitter->setNLLOffset(_nllOffset);
  fitter->setTolerance(_minTolerance);
  fitter->setStrategy(_minStrategy);
  fitter->setOptConst(_optConst);
  fitter->setPrintLevel(_printLevel);
  fitter->setNCPU(_nCPU);
  fitter->setOutputFile((TString)_outputFile);
  fitter->setSnapshotName((TString)_ssname);
  fitter->saveWorkspace(_saveWS);
  fitter->saveErrors(_saveErrors);
  fitter->setFixStarCache(_fixStarCache);
  fitter->setExternalConstraint(&externalConstrSet);
  fitter->setParamList(&fitPOIs);
  fitter->setGKIntegrator(_GKIntegrator);
  fitter->setSaveNP(_saveNP);
  fitter->saveFitResult(_saveFitResult);
  fitter->setStorageLevel(_storageLevel);
  fitter->setErrorLevel(_errorLevel);
  fitter->setSumW2(_setSumW2);
  fitter->setRangeName(_rangeName);
  fitter->setSaveHesse(_saveHesse);
  fitter->setFitHesse(_fitHesse);

#if ROOT_VERSION_CODE >= ROOT_VERSION(6, 24, 3)
  fitter->setBatchMode(_useBatch);
  fitter->setSamplingRelTol(_samplingRelTol);
#endif
  // Sanity checks on model
  if (_checkWS)
  {
    spdlog::info("Performing sanity checks on model...");
    bool validModel = fitter->checkModel(*mc, true);
    spdlog::info("Sanity checks on the model: {}", (validModel ? "OK" : "FAIL"));
  }

  // Fitting
  TStopwatch timer;
  spdlog::info("Starting fit...");
  int status = fitter->profileToData(mc, data); // Perform fit
  timer.Stop();
  double t_cpu_ = timer.CpuTime() / 60.;
  double t_real_ = timer.RealTime() / 60.;
  spdlog::info("All fits done in {} min (cpu), {} min (real)", t_cpu_, t_real_);

  std::string STATMSG = (status) ? "\033[91m STATUS FAILED \033[0m" : "\033[92m STATUS OK \033[0m";

  // Print summary
  spdlog::info("  Fit Summary of POIs ({})", STATMSG.c_str());
  spdlog::info("------------------------------------------------");

  for (RooLinkedListIter it = fitPOIs.iterator(); RooRealVar *POI = dynamic_cast<RooRealVar *>(it.Next());)
  {
    POI->Print();
  }

  if (status)
  {
    spdlog::error("*****************************************");
    spdlog::error("       WARNING: Fit status failed.       ");
    spdlog::error("*****************************************");
  }

  tf->Close();
  return 0;
}

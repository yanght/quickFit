#include "CommonHead.h"
#include "RooFitHead.h" 
#include "RooStatsHead.h"

#include <boost/program_options.hpp>

#include "auxUtils.h"
#include "utils.h"

std::string _outputFile = "";
std::string _inputFile = "";
std::string _dataName = "toyData";
std::string _wsName = "combWS";
std::string _mcName = "ModelConfig";
std::string _snapshot = "";

std::string _poiStr = "";
bool _binned = false;
bool _randomizeGlobObs = true;
int _seed = 0;

int main( int argc, char** argv )
{
  namespace po = boost::program_options;
  po::options_description desc( "quickFit options" );
  desc.add_options()
    // IO Options 
    ( "inputFile,f",   po::value<std::string>(&_inputFile)->required(),  "Specify the input TFile (REQUIRED)" )

    ( "outputFile,o",  po::value<std::string>(&_outputFile)->required(), "Save fit results to output TFile (REQUIRED)" )

    ( "dataName,d",    po::value<std::string>(&_dataName)->default_value(_dataName),   
                         "Name of the dataset" )
    ( "wsName,w",      po::value<std::string>(&_wsName)->default_value(_wsName),
                         "Name of the workspace" )
    ( "mcName,m",      po::value<std::string>(&_mcName)->default_value(_mcName), 
                         "Name of the model config" )
    ( "snapshot,s",    po::value<std::string>(&_snapshot)->default_value(_snapshot), 
                         "Load snapshot from workspace" )
    ( "poi,p",         po::value<std::string>(&_poiStr)->default_value(_poiStr),     "Specify POIs to be used in fit" )

    ( "seed",         po::value<int>(&_seed)->required(),     "Random seed (REQUIRED)" )
    ( "binned,b",         po::value<bool>(&_binned)->default_value(_binned),     "Generate binned toy" )
    ( "randomizeGlobObs,r",         po::value<bool>(&_randomizeGlobObs)->default_value(_randomizeGlobObs),     "Randomize global observables" )
    
    ( "help,h",          "Print help message")
    ;

  po::variables_map vm;
  try
  {
    po::store( po::command_line_parser( argc, argv ).options( desc ).run(), vm );
    po::notify( vm );
  }
  catch ( std::exception& ex )
  {
    std::cerr << "Invalid options: " << ex.what() << std::endl;
    std::cout << "Invalid options: " << ex.what() << std::endl;
    std::cout << "Use manager --help to get a list of all the allowed options"  << std::endl;
    return 999;
  }
  catch ( ... )
  {
    std::cerr << "Unidentified error parsing options." << std::endl;
    return 1000;
  }

  // if help, print help
  if ( !vm.count("inputFile") || vm.count( "help" ) )
  {
    std::cout << "Usage: manager [options]\n";
    std::cout << desc;
    return 0;
  }

  RooMsgService::instance().getStream(1).removeTopic(RooFit::NumIntegration) ;
  RooMsgService::instance().getStream(1).removeTopic(RooFit::Fitting) ;
  RooMsgService::instance().getStream(1).removeTopic(RooFit::Minimization) ;
  RooMsgService::instance().getStream(1).removeTopic(RooFit::InputArguments) ;
  RooMsgService::instance().getStream(1).removeTopic(RooFit::Eval) ;
  RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR);
  
  // Get workspace, model, and data from file
  TFile *tf = TFile::Open( (TString) _inputFile );
  if (not tf->IsOpen()) {
    std::cout << "Error: TFile \'" << _inputFile << "\' was not found." << endl;
    return 1;
  }

  RooWorkspace *ws = (RooWorkspace*)tf->Get( (TString) _wsName );
  if (ws == nullptr) {
    std::cout << "Error: Workspace \'" << _wsName << "\' does not exist in the TFile." << endl;
    return 1;
  }
  
  RooStats::ModelConfig *mc = (RooStats::ModelConfig*)ws->obj( (TString) _mcName );
  if (mc == nullptr) {
    std::cout << "Error: ModelConfig \'" << _mcName << "\' does not exist in workspace." << endl;
    return 1;
  }
  
  utils::loadSnapshot(ws, _snapshot);
  // Prepare parameters of interest
  RooArgSet fitPOIs;
  utils::setupPOI(mc, _poiStr, &fitPOIs);
    
  cout<<endl<<"Summary of POI setup in the likelihood model"<<endl;
  fitPOIs.Print("v");

  TStopwatch timer;
  cout << endl << "Starting generating toy..." << endl;
  
  // Generate toy
  if(_binned){
    RooSimultaneous *m_pdf = dynamic_cast<RooSimultaneous*>(mc->GetPdf());
    RooCategory*  m_cat = (RooCategory*)(&m_pdf->indexCat());
    int numChannels = m_cat->numBins(0);
    for ( int i= 0; i < numChannels; i++ ) {
      m_cat->setBin(i);
      TString channelname=m_cat->getLabel();
      RooAbsPdf* pdfi = m_pdf->getPdf(channelname);
      pdfi->setAttribute("PleaseGenerateBinned");
    }
  }
  
  // mc->GetGlobalObservables()->Print("V");
  if(_randomizeGlobObs) {
    utils::randomizeSet(mc->GetPdf(), const_cast<RooArgSet*>(mc->GetGlobalObservables()), _seed);
  }
  // mc->GetGlobalObservables()->Print("V");
  
  unique_ptr<RooDataSet> toyData;
  if(_binned) toyData.reset((mc->GetPdf()->generate(*mc->GetObservables(), Extended(), AutoBinned(kTRUE), GenBinned("PleaseGenerateBinned"))));
  else toyData.reset((mc->GetPdf()->generate(*mc->GetObservables(), Extended(), AutoBinned(kTRUE))));
  toyData->SetName(_dataName.c_str());
  ws->import(*toyData.get());
  
  timer.Stop();
  double t_cpu_ = timer.CpuTime()/60.;
  double t_real_ = timer.RealTime()/60.;
  printf("\nAll operations done in %.2f min (cpu), %.2f min (real)\n", t_cpu_, t_real_);
  // Print summary
  ws->writeToFile(_outputFile.c_str());
  
  cout << endl << "  Workspace with toy dataset (" << _dataName << ") saved in output file "
       << _outputFile << endl;
  return 0;
}

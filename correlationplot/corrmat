#!/usr/bin/env python
# $Id: corrmat,v 1.21 2018/07/05 15:12:32 adye Exp $

__author__  = "Tim Adye"
__version__ = "$Revision: 1.21 $"

import sys, os, getopt, re, math, optparse
from sys import argv, exit, stdout
from os.path import dirname, basename
from math import sqrt, pi, copysign
from array import array
from copy import deepcopy

import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions= True   # prevent ROOT screwing with our help text
from ROOT import RooFit, gStyle

try:    import ld
except: pass

# Enable Python-style iteration over RooAbsCollection
def TIterator_next(self):
  v=self.Next()
  if v: return v
  raise StopIteration()
ROOT.TIterator.next= TIterator_next  # handles all TIterators, including RooLinkedListIter, returned by RooAbsCollection.iterator()
ROOT.TIterator.__iter__= lambda self: self
ROOT.RooAbsCollection.__iter__= lambda self: self.iterator()
def qw(s): return tuple(s.split())

prog= basename(argv[0])
opt=None
verbose=0
__version__ = re.sub(r"^.* ([\d.]+).*$",r"\1",__version__)

excluded_pois= r"^mu_XS_(WH|ZH|ttHtH)_BR_ZZ$"
option_defaults= {  # defaults for each model. NB. can disable defaulted flags with --no_xsbr or --no_exclude or --no_group
  "A1_?5P$":   { "zlabel": "#mu_{i}, #mu_{j}" },
  "A1_?5D":    { "zlabel": "#mu^{i}, #mu^{j}" },
  "A1_?5PD":   { "xsbr":True, "exclude":True, "group":True, "zlabel": "#sigma_{i}#timesB^{f}, #sigma_{j}#timesB^{k}" },
  "A1_?5P78":  { "group":True, "zlabel": "#mu_{i}^{xTeV}, #mu_{j}^{yTeV}"},
  "B1(ZZ|WW)": { "xsbr":True },
  "(D1$|D1_?general)": { "mu_ggF":True, "group":True, "zlabel": "#lambda_{i}^{f} or #mu_{#scale[0.8]{ggF}}^{f}, #lambda_{j}^{k} or #mu_{#scale[0.8]{ggF}}^{k}" },
  "(D1_R1|D1_?rank1)": { "mu_ggF":True },
}
sortPoi=      qw("XS BR V F ZZ_r WW_r kappa_gZ mu l lambda kappa BRinv ggF ggFbbH VBF WH ZH ttH ttHtH gamgam ZZ WW tautau bb Z W t tau b g gam gZ Zg tg WZ gamZ tauZ bZ du Vu lq Vq")
# ATLAS paper order
sortPoiAtlas= qw("XS BR V F ZZ_r WW_r kappa_gZ mu l lambda kappa BRinv ggF ggFbbH VBF WH ZH ttH ttHtH gamgam ZZ WW tautau bb Z W t tau b g gam gZ Zg WZ tg bZ tauZ gamZ du Vu lq Vq")


def parseArgs():
  global parser, opt, verbose
  # parser.boolean() defines --FLAG and --no_FLAG
  parser= MyOptParse(usage="%prog [OPTIONS] ws.root [ws2.root...]", version="%prog "+__version__)
  parser.add_option ("-v", "--verbose",      help="verbose running",       action="count", default=1)
  parser.add_option ("-q", "--quiet",        help="quiet running",         action="count", default=0)
  parser.boolean    ("-i", "--interactive",  help="ROOT interactive mode")
  parser.add_option ("-r", "--result",       help="HESSE result object name, or 2D histogram (eg. h_cor for Andrew's file)", default="fitres/data_stage1_hesse,fitted_poicov,h_cor")
  parser.add_option ("-o", "--output",       help="Outfile file", metavar="FILE")
  parser.add_option ("-d", "--dp",           help="Decimal places to print (default=2)", type="int")
  parser.boolean    ("-a", "--atlas_order")
  parser.boolean    ("-A", "--asimov")
  parser.add_option ("-p", "--pois",         help="POI selections", default="mu*,kappa_*,lambda_*,l_*")
  parser.add_option ("-P", "--exclude_pois", help="exclude specified pois")
  parser.boolean    ("-x", "--xsbr")
  parser.boolean    ("-D", "--mu_ggF",       help="mu_XX is actually mu^XX_ggF")
  parser.boolean    ("-X", "--exclude")
  parser.boolean    ("-b", "--blank")
  parser.boolean    ("-g", "--group",        help="group POIs by XS. Implies --xsbr")
  parser.add_option ("-z", "--zlabel",       default="X,Y")
  parser.boolean    ("-R", "--ratio",        help="show ratio of covariance matrices (specify two workspace files)")
  parser.add_option ("-N", "--no_defaults",  help="don't take model-specific defaults",action="store_true")
  parser.add_option ("-m", "--model",        help="model name")
  parser.add_option ("-s", "--sort_impact",  help="sort by correlation with POI", metavar="POI")
  parser.add_option ("-n", "--num_pois",     help="include only first NUM POIs", metavar="NUM", type="int")
  parser.add_option ("-M", "--margin",       help="margin size", type="float")
  parser.add_option (      "--preliminary",  action="store_true", default=False)
  parser.add_option (      "--internal",     action="store_true", default=False)
  parser.add_option ("-L", "--no_label",     action="store_true", default=False)
  parser.boolean    ("-V", "--variance",     help="show mean and RMS of multiple toy files")
  parser.boolean    ("-%", "--percent",      help="show percentage correlation")
  opt, args= parser.parse_args()
  verbose= opt.verbose - opt.quiet
  if opt.dp is None:
    if opt.percent: opt.dp= 0
    else:           opt.dp= 2
  if opt.output and len(args)>1 and not opt.ratio and not opt.variance:
    print(prog+": cannot specify -o with multiple inputs")
    exit(3)
  if len(args)<1 or (opt.ratio and len(args)!=2):
    parser.print_help()
    print("\nDefault options are set for some models, and default flags can be disabled with --no_FLAG")
    exit(1)
  return args

class Error(Exception):
  def __init__(self,stat): self.stat= stat

class MyOptParse (optparse.OptionParser):
  # boolean() automatically defines --longopt and --no_longopt
  def boolean (self, shortopt, longopt, **kwargs):
    dest= re.sub (r"^--", "", longopt)
    self.add_option (shortopt, longopt, dest=dest, action="store_true",  **kwargs)
    kwargs["help"]= optparse.SUPPRESS_HELP
    self.add_option ("--no_"+dest,      dest=dest, action="store_false", **kwargs)

def PlotFile(canvas,plotFile,last=True):
  if not ROOT.gROOT.IsBatch():
    canvas.Update();
    canvas.WaitPrimitive();
  canvas.Print (plotFile)
  if last:
    canvas.Print (re.sub(r"\.[^.]+$",r".C",plotFile))
    canvas.Print (re.sub(r"\.[^.]+$",r".eps",plotFile))
    canvas.IsA().Destructor(canvas)


def poiGroup(p,model):
  varsubs= []
  if opt.mu_ggF: varsubs += [
    [r"mu_[^_]+", r"ggF"],
  ]
  varsubs += [
    [r"mu_XS_([^_]+)_BR_[^_]+"],
    [r"l_([^_]+)_[^_]+"],
    [r"mu_XS([78])_[^_]+", r"\1TeV"],
  ]
  for s in varsubs:
    if len(s)==1: s.append(r"\1")
    g, found= re.subn (r"^"+s[0]+"$", s[1], p)
    if found: return g
  print("POI",p,"is incompatible with --group")
  raise Error(6)

def niceName(name):
  varsubs= []
  if opt.exclude: varsubs += [
    [excluded_pois, ""],
  ]
  if opt.exclude_pois: varsubs += [
    ["|".join([wild2re(p) for p in opt.exclude_pois.split(",") if p!=""]), ""],
  ]
  if opt.mu_ggF: varsubs += [
    [r"^mu_([^_]+)$", r"mu_XS_ggF_BR_\1"],
  ]
  if opt.xsbr: varsubs += [
#    [r"^mu_XS_ggF_x_BR_([^_]+)$", r"#splitline{#sigma(gg#rightarrow}{H#rightarrow\1)}"],
    [r"^mu_XS_ggF_x_BR_([^_]+)$", r"#sigma(gg#rightarrowH#rightarrow\1)"],
    [r"^mu_XS_([^_]+)_r_XS_([^_]+)$", r"#sigma_{\1}/#sigma_{\2}"],
    [r"^mu_BR_([^_]+)_r_BR_([^_]+)$", r"B^{\1}/B^{\2}"],
  ]
  if opt.group: varsubs += [
    [r"^mu_XS_[^_]+_BR_([^_]+)$", r"\1"],
    [r"^l_[^_]+_([^_]+)$", r"\1"],
    [r"^mu_XS[78]_([^_]+)$", r"\1"],
  ]
  elif opt.xsbr: varsubs += [
    [r"^mu_XS_([^_]+)_BR_([^_]+)$", r"{}^{\2}_{\1}"],
  ]
  varsubs += [
    #[r"^mH$", r"m_{H}"],
    #[r"^mH_ZZ_(run[12])$", r"m_{\1}^{4#font[12]{l}}"],
    #[r"^mH_gg_(run[12])$", r"m_{\1}^{gamgam}"],
    #[r"^ATLAS_", r""],
    #[r"^kappa_mu$", r"#kappa_{#mu}"],
    #[r"^kappa_([^_]+)_([^_]+)$", r"#kappa_{\1}^{\2}"],
    #[r"^(kappa|lambda)_([^_]+)$", r"#\1_{\2}"],
    #[r"^mu_XS_([^_]+)(?:_x)?_BR_([^_]+)$", r"#mu_{\1}^{\2}"],
    #[r"^mu_XS([78])_r_XS([78])_([^_]+)$", r"#mu_{\3}^{\1TeV}/#mu_{\3}^{\2TeV}"],
    #[r"^mu_XS_([^_]+)_r_XS_([^_]+)$", r"#mu_{\1}/#mu_{\2}"],
    #[r"^mu_BR_([^_]+)_r_BR_([^_]+)$", r"#mu^{\1}/#mu^{\2}"],
    #[r"^mu_XS([78])_([^_]+)$", r"#mu_{\2}^{\1TeV}"],
    #[r"^mu_V_r_F_([^_]+)$", r"#mu_{V}^{\1}/#mu_{F}^{\1}"],
    #[r"^mu_V_r_F$", r"#mu_{V}/#mu_{F}"],
    #[r"^mu_([FV])_([^_]+)$", r"#mu_{\1}^{\2}"],
    #[r"^mu_([FV])$", r"#mu_{\1}"],
    #[r"^mu_BR_([^_]+)$", r"#mu^{\1}"],
    #[r"^mu_XS_([^_]+)$", r"#mu_{\1}"],
    #[r"^mu$", r"#mu"],
    #[r"^mu_([^_]+)$", r"#mu^{\1}"],
    #[r"^l_([^_]+)_([^_]+)$", r"#lambda_{\1}^{\2}"],
    #[r"^l_([^_]+)$", r"#lambda_{\1}"],
    #[r"^eps$", r"#epsilon"],
    #[r"gam", r"#gamma", 0],
    #[r"tau", r"#tau", 0],
#    [r"\bbb\b", r"b#bar{b}", 0],
    [r"mumu", r"#mu#mu ",0],
    [r"ggFbbH", r"ggF", 0],
    [r"ttHtH", r"ttH", 0],
    [r"^BRinv$", r"B_{BSM}"],
    [r"^pdf_Higgs_gg$", r"#scale[0.7]{PDF ggF}"],
    [r"^QCDscale_ggH$", r"#scale[0.7]{QCD ggF}"],
    [r"r_gg2H_0J", r"#it{gg#rightarrowH}, 0-jet"],
    [r"r_gg2H_1J_ptH_0_60", r"#it{gg#rightarrowH}, 1-jet, #it{p_{T}^{H}} < 60 GeV"],
    [r"r_gg2H_1J_ptH_60_120", r"#it{gg#rightarrowH}, 1-jet, 60 #leq #it{p_{T}^{H}} < 120 GeV"],
    [r"r_gg2H_1J_ptH_120_200", r"#it{gg#rightarrowH}, 1-jet, 120 #leq #it{p_{T}^{H}} < 200 GeV"],
    [r"r_gg2H_ge1J_ptH_gt200", r"#it{gg#rightarrowH}, #geq 1-jet, #it{p_{T}^{H}} #geq 200 GeV"],
    [r"r_gg2H_ge2J_ptH_0_200", r"#it{gg#rightarrowH}, #geq 2-jet, #it{p_{T}^{H}} < 200 GeV"],
    [r"r_qq2Hqq_nonVH2jet", r"#it{qq#rightarrowHqq}, no #it{VH}-like"],
    [r"r_qq2Hqq_VH2jet", r"#it{qq#rightarrowHqq}, #it{VH}-like"],
    [r"r_qq2Hqq_pTjet1_gt200", r"#it{qq#rightarrowHqq}, #it{p_{T}^{j}} #geq 200 GeV"],
    [r"r_qq2Hlnu_pTV_0_250", r"#it{qq#rightarrowHl#nu}, #it{p^{V}_{T}} < 250 GeV"],
    [r"r_qq2Hlnu_pTV_gt250", r"#it{qq#rightarrowHl#nu}, #it{p^{V}_{T}} #geq 250 GeV"],
    [r"r_ZHll_pTV_0_150", r"#it{gg/qq#rightarrowHll}, #it{p^{V}_{T}} < 150 GeV"],
    [r"r_ZHll_pTV_150_250", r"#it{gg/qq#rightarrowHll}, 150 #leq #it{p^{V}_{T}} < 250 GeV"],
    [r"r_ZHll_pTV_gt250", r"#it{gg/qq#rightarrowHll}, #it{p^{V}_{T}} #geq 250 GeV"],
    [r"r_ttH", r"#it{ttH + tH}"],
    [r"r_BR_yy_o_ZZ", r"#it{B_{#gamma#gamma}/B_{ZZ}}"],
    [r"r_BR_bb_o_ZZ", r"#it{B_{b#bar{b}}/B_{ZZ}}"],
    [r"r_BR_WW_o_ZZ", r"#it{B_{WW}/B_{ZZ}}"],
    [r"r_BR_tautau_o_ZZ", r"#it{B_{#tau^{+}#tau^{-}}/B_{ZZ}}"],
  ]
  for s in varsubs:
    s += [1] # default count
    name= re.sub (s[0], s[1], name, count=s[2])
    if name=="": return name
  return name

def niceNum(c):
  ct= ("%."+str(max(0,(2+opt.dp-len("%+.0f"%c))))+"f") % c
  ct= re.sub(r"^-(0\.?0*)$",r"\1",ct)
  if ct[0]=="-": ct= "#minus"+ct[1:]
  return ct

def NewTLatex (ndc=True, align=11, size=0.03, color=None, font=42):
  if color is None: color= ROOT.kBlack
  t= ROOT.TLatex()
  t.SetTextAlign(align)
  t.SetTextSize(size)
  if ndc: t.SetNDC()
  t.SetTextColor(color)
  t.SetTextFont(font)   # TLatex default is 62 (helvetica-bold-r-normal). Want medium instead.
  return t

def wild2re(w):
  if w==None or w=="": return None
  return "^(" + re.sub (r",", r"|", re.sub (r"\*", r".*", re.sub (r"\?", r".", re.sub (r"([.(){}^$|+\\])", "\\\1", w)))) + ")$"

def poikey(s,l):
  for i,k in enumerate(l):
    s= re.sub (r"(^|_)"+k+r"(_|$)", r"\g<1>"+("%02d"%i)+r"\g<2>", s)
  return s

def set_defaults(model):
  global opt
  if opt.no_defaults: return
  newparser= deepcopy(parser)
  allopt= []
  for m,ol in list(option_defaults.items()):
    if re.search(m,model):
      newparser.set_defaults(**ol)
      for o,ov in list(ol.items()):
        if type(ov) is bool:
          if ov: allopt.append("--"+o)
          else:  allopt.append("--no_"+o)
        else:
          allopt.append("--"+o+"="+repr(ov))
  if allopt: print("Model",model,"default options:",(" ".join(allopt)))
  opt, dummy= newparser.parse_args()
  if opt.dp is None:
    if opt.percent: opt.dp= 0
    else:           opt.dp= 2


def process(args):
  if not opt.interactive: ROOT.gROOT.SetBatch(True)
  ROOT.TH1.AddDirectory(0)
  stat=0
  if opt.variance:
    return variance(args)
  if opt.ratio:
    try: hinfo= corrmat(args[0],args[1])
    except Error as e: stat=e.stat
    plotmat(*hinfo)
  else:
    for f in args:
      try: hinfo=corrmat(f)
      except Error as e: stat=e.stat
      plotmat(*hinfo)
  return stat

def variance(args):
  hlist=[]
  stat=0
  hinfo=None
  for i,f in enumerate(args):
    try: hinfo2= corrmat(f,None)
    except Error as e: stat=e.stat
    hlist.append(hinfo2[0])
    if hinfo is None: hinfo=hinfo2
  if not hlist:
    print("No correlation matrices")
    return stat
  h= hlist[0]
  n= h.GetNbinsX()
  vlist=[]
  for i in range(n):
    rlist=[]
    for j in range(i,n):
      rlist.append (ROOT.TH1D ("var_%d_%d"%(i+1,j+1), "Variance for #rho(%s,%s)"%(h.GetXaxis().GetBinLabel(i+1),h.GetYaxis().GetBinLabel(n-j)), 202, -1.01, 1.01))
    vlist.append(rlist)

  for h in hlist:
    for i in range(n):
      for j in range(i,n):
        vlist[i][j-i].Fill (h.GetBinContent(i+1,n-j))

  h2= h.Clone(h.GetName()+"_var")
  for i in range(n):
    for j in range(i,n):
      h2.SetBinContent (i+1, n-j, vlist[i][j-i].GetMean())
      h2.SetBinError   (i+1, n-j, vlist[i][j-i].GetRMS())
      if i!=j:
        h2.SetBinContent (j+1, n-i, vlist[i][j-i].GetMean())
        h2.SetBinError   (j+1, n-i, vlist[i][j-i].GetRMS())

  output, model= hinfo[1:3]
  canvas= ROOT.TCanvas (model+"-hists", model, 800, 700)
  canvas.Print (output+"[")
  plotmat(h2,*hinfo[1:])

  gStyle.SetOptTitle(1)
  gStyle.SetOptStat(1)
  gStyle.SetOptStat(1110);
  gStyle.SetStatX(0.9);
  gStyle.SetStatY(0.9);
  for rlist in vlist:
    for h in rlist[1:]:
      h.Draw()
      PlotFile(canvas,output,False)
  canvas.Print (output+"]")

  if opt.output:
    f= ROOT.TFile.Open (re.sub(r"\.[^.]+$",r".root",opt.output), "recreate")
    for h in hlist:
      f.WriteTObject(h)
    for rlist in vlist:
      for h in rlist:
        f.WriteTObject(h)
    del f

  return stat
    
def read_corrmat(fpath,second=False):
  if opt.model is None:
    model= re.sub(r"_data.*$","",re.sub(r"\.root$","",basename(fpath)))
    if opt.asimov: isAsimov= True
    else:          model, isAsimov= re.subn(r"_asimov.+$","",model)
  else:
    model, isAsimov= opt.model, opt.asimov
  if not second: set_defaults(model)

  f= ROOT.TFile.Open (fpath)
  if not f: raise Error(1)

  for resname in opt.result.split(","):
    res= f.Get(resname)
    if res: break
  if not res:
    print("No result object found named",opt.result)
    raise Error(2)

  if isinstance(res,ROOT.TNamed): resname= res.GetName()
  if verbose>=0: print(res.ClassName()+"::"+resname,"in file",fpath+", model",model)
  if   isinstance(res,ROOT.RooFitResult):
    restype= 0
    poilist= res.floatParsFinal().selectByName(opt.pois+("" if opt.sort_impact is None else ","+opt.sort_impact)).selectByAttrib("Constant",0)
    poiname= [p.GetName() for p in poilist]
  elif isinstance(res,getattr(ROOT,"TMatrixTSym<double>")):
    restype= 1
    poilist= f.Get("fitted_poi")
    if not poilist or not isinstance(poilist,ROOT.RooArgSet):
      print(res.GetName(),"requires also fitted_poi")
      raise Error(2)
    poilist= ROOT.RooArgList(poilist).selectByAttrib("Constant",0)
    if poilist.getSize()!=res.GetNrows():
      print(res.GetName(),"size is",res.GetNrows(),"but",poilist.GetName(),"size is",poilist.getSize())
      raise Error(2)
    poiname= [p.GetName() for p in poilist]
  elif isinstance(res,ROOT.TH2D):  # 2D histogram from Andrew: specify h_cor (or h_cov for --ratio)
    restype= 2
    ax= res.GetXaxis()
    ay= res.GetYaxis()
    poiname= [ax.GetBinLabel(i+1) for i in range(ax.GetNbins())]
  else:
    print(res.GetName(),"wrong type")
    raise Error(2)

  if opt.sort_impact is not None:
    isort= poiname.index(opt.sort_impact)
    if   restype==0:
      poiIndex= [res.floatParsFinal().index(p) for p in poiname]
      varerr= [abs(res.correlationMatrix()(poiIndex[i], poiIndex[isort])) for i in range(len(poiIndex))]
    elif restype==1:
      poiIndex= [poilist.index(p) for p in poiname]
      vararray= []
      for i in range(len(poiIndex)):
        v= res(poiIndex[i], poiIndex[i]) * res(poiIndex[isort], poiIndex[isort])
        if v<=0.0: c= 0.0
        else:      c= abs(res (poiIndex[i], poiIndex[isort])) / sqrt(v)
        varerr.append(c)
    elif restype==2:
      poiIndex= [(ax.FindBin(p), ay.FindBin(p)) for p in poiname]
      varerr= [abs (res.GetBinContent (poiIndex[i][0], poiIndex[isort][1])) for i in range(len(poiIndex))]
    poierr= list(zip(poiname,varerr))
    if verbose>=1:
      for p,e in poierr: print("%.3f %s" % (copysign(sqrt(abs(e)),e), p))
    poiname= [p[0] for p in sorted (poierr, key=lambda p: p[1], reverse=True)]
  else:
      1
    #poiname= sorted (poiname, key=lambda p: poikey (p, sortPoiAtlas if opt.atlas_order else sortPoi)) #turn off
  if opt.num_pois is not None: poiname= poiname[:opt.num_pois]
  poilatex= [niceName(p) for p in poiname]
  excl=                                     [p[0] for p in zip(poiname, poilatex) if p[1]==""]
  poiname, poilatex= [list(p) for p in zip(*[p    for p in zip(poiname, poilatex) if p[1]!=""])]
  if excl: print("Exclude POIs:",(" ".join(excl)))

  if   restype==0:
    poiIndex= [res.floatParsFinal().index(p) for p in poiname]
  elif restype==1:
    poiIndex= [poilist.index(p) for p in poiname]
  elif restype==2:
    poiIndex= [(ax.FindBin(p), ay.FindBin(p)) for p in poiname]
  if restype!=2 and verbose>=1:
    poi= ROOT.RooArgList()
    for p in poiname: poi.add(poilist.find(p))
    poi.Print("v")
  return res, model, isAsimov, poiIndex, poiname, poilatex, restype

def corrmat(fpath,fpath2=None):
  res, model, isAsimov, poiIndex, poiname, poilatex, restype= read_corrmat(fpath)
  if opt.ratio and fpath2:
    res2, model2, isAsimov2, poiIndex2, poiname2, poilatex2, restype= read_corrmat(fpath2,True)
    if isAsimov==isAsimov2:
      print("Warning: Need to specify one observed and one expected file with --ratio. Assume",fpath,"is observed,",fpath2,"is expected")
      isAsimov, isAsimov2= 0, 1
    elif isAsimov:
      res2, model2, isAsimov2, poiIndex2, poiname2, poilatex2,  res, model,  isAsimov,  poiIndex,  poiname,  poilatex,  restype= \
      res,  model,  isAsimov,  poiIndex,  poiname,  poilatex,  res2, model2, isAsimov2, poiIndex2, poiname2, poilatex2, restype2
    if model!=model2: print("Warning: different models:",model,"and",model2)
    if poiname!=poiname2 or restype!=restype2:
      print("Different POIs:",(", ".join(poiname)),"and",(", ".join(poiname2)))
      raise Error(4)
  if   opt.ratio:                    model += "_ratio"
  elif isAsimov and not opt.asimov:  model += "_asimov"
  n= len(poiname)
  maxlen= max ([len(p) for p in poilatex])
  if opt.blank: blanked= [re.search(excluded_pois,p) for p in poiname]
  else:         blanked= [False] * n
  if verbose>=1: print(" ".join(poilatex)," maxlen=",maxlen,"n=",n)

  h= ROOT.TH2D (model,model+" correlation matrix", n, 0, n, n, 0, n)
  ax= h.GetXaxis()
  ay= h.GetYaxis()
  for i in range(n):
    ax.SetBinLabel(i+1,poilatex[i])
    ay.SetBinLabel(n-i,poilatex[i])
    for j in range(i+1):
      if blanked[i] or blanked[j]:
        if i==j or opt.ratio: c= 1.0
        else:                 c= 0.0
      elif opt.ratio:
        if   restype==0:
          c1= res. covarianceMatrix()(poiIndex[i],    poiIndex[j])
          c2= res2.covarianceMatrix()(poiIndex[i],    poiIndex[j])
        elif restype==1:
          c1= res                    (poiIndex[i],    poiIndex[j])
          c2= res2                   (poiIndex[i],    poiIndex[j])
        elif restype==2:
          c1= res. GetBinContent     (poiIndex[i][0], poiIndex[j][1])
          c1= res2.GetBinContent     (poiIndex[i][0], poiIndex[j][1])
        c= sqrt(abs(c1/c2))
        if c1/c2<0.0: c= -c
        if verbose>=1: print("%-30s %-30s %7.4f / %7.4f = %7.4f" % (poiname[i], poiname[j], c1, c2, c))
      else:
        if   restype==0:
          c= res.correlationMatrix()(poiIndex[i],    poiIndex[j])
        elif restype==1:
          v= res(poiIndex[i], poiIndex[i]) * res(poiIndex[j], poiIndex[j])
          if v<=0.0:
            print("Bad covariances %g * %g" % (res(poiIndex[i], poiIndex[i]), res(poiIndex[j], poiIndex[j])))
            c= 0.0
          else:
            c= res                  (poiIndex[i],    poiIndex[j]) / sqrt(v)
        elif restype==2:
          c= res.GetBinContent      (poiIndex[i][0], poiIndex[j][1])
        if verbose>=1: print("%-30s %-30s %7.4f" % (poiname[i], poiname[j], c))
      h.SetBinContent (i+1, n-j, c)
      if i!=j: h.SetBinContent (j+1, n-i, c)

  if opt.output: output= opt.output
  else:          output= prog+"_"+model+".pdf"

  return h, output, model, isAsimov, poiname, blanked, maxlen
  

def plotmat(h, output, model, isAsimov, poiname, blanked, maxlen):
  ax= h.GetXaxis()
  ay= h.GetYaxis()
  az= h.GetZaxis()
  n=  h.GetNbinsX()

  gStyle.SetOptStat(0)
  gStyle.SetOptTitle(0)
  gStyle.SetPaintTextFormat(".2f")
  # palette going from orange (-1) to white (0) to blue (1)
  ROOT.TColor.CreateGradientColorTable (3,
                                        array ("d", [0.00, 0.50, 1.00]), # stops (0..1)
                                        array ("d", [1.00, 1.00, 0.00]), # red
                                        array ("d", [0.70, 1.00, 0.34]), # green
                                        array ("d", [0.00, 1.00, 0.82]), # blue
                                        255,  1.0)

  alab= (opt.zlabel=="X,Y")
  if opt.ratio:
    ztitle= "#sqrt{V}_{obs}("+opt.zlabel+") / #sqrt{V}_{exp}("+opt.zlabel+")"
  else:
    ztitle= "#rho("+opt.zlabel+")"
    if opt.percent: ztitle += " %"
  print(ztitle)
  bigz= bool(re.search(r"_\{[^}]+\}\^\{",ztitle) or re.search(r"#sqrt\{[^}]+\}_\{",ztitle))
  canvas= ROOT.TCanvas (model, model, 800, 700)
  canvas.SetFillStyle(0);
  canvas.SetTicks(1,1)   # we don't actually draw any ticks, but specifying that top+right axis ticks should be drawn seems to work round a bug where the axes aren't drawn or drawn very thin.
  #if opt.no_label: canvas.SetTopMargin(0.02) #turn off
  if alab:
    if opt.margin is not None: canvas.SetLeftMargin(opt.margin)
    elif maxlen>22:            canvas.SetLeftMargin(0.16)
    else:                      canvas.SetLeftMargin(0.12)
    canvas.SetRightMargin(0.14)
    if opt.margin is not None: canvas.SetBottomMargin(opt.margin+0.035)
    else:                      canvas.SetBottomMargin(0.14)
  elif opt.group:
    if opt.margin is not None:
      canvas.SetLeftMargin(opt.margin)
      canvas.SetBottomMargin(opt.margin)
    else:
      canvas.SetLeftMargin(0.13)
      canvas.SetBottomMargin(0.11)
    if bigz: canvas.SetRightMargin(0.16)
    else:    canvas.SetRightMargin(0.14)
  else:
    if opt.margin is not None:
      canvas.SetLeftMargin(opt.margin)
      canvas.SetBottomMargin(opt.margin)
    else:
      canvas.SetLeftMargin(0.09)
      canvas.SetBottomMargin(0.09)
    canvas.SetRightMargin(0.16)

  if   n>40:                   labsiz=0.020
  elif n>16:                   labsiz=0.025
  elif maxlen>22 or opt.group: labsiz=0.04
  else:                        labsiz=0.07
  if verbose>=1: print("n=",n,"maxlen=",maxlen,"labsiz=",labsiz,"alab=",alab,"bigz=",bigz)
  if opt.ratio: hmin, hmax= -1.0, 3.0
  else:         hmin, hmax= -1.0, 1.0
  h.SetMinimum(hmin)
  h.SetMaximum(hmax)
  h.SetContour(255)
  if   labsiz<0.039:
    ax.SetLabelOffset(0.002)
    ay.SetLabelOffset(0.002)
  elif labsiz<0.05:  ax.SetLabelOffset(0.008)
  else:              ax.SetLabelOffset(0.012)
  if opt.no_label: ax.LabelsOption("v")
  if alab:
    if not opt.no_label:
      ax.SetTitle("Parameter X")
      ax.SetTitleOffset(1.7)
      if maxlen>22:
        ay.SetTitle("Parameter Y        ")
        ay.SetTitleOffset(2.3)
      else:
        ay.SetTitle("Parameter Y")
        ay.SetTitleOffset(1.7)
  elif opt.group:
    zl= [l.strip() for l in opt.zlabel.split(",")]
    ax.SetTitle(zl[0])
    ay.SetTitle(zl[1])
    if opt.group:
      ax.LabelsOption("h")
      ax.SetTitleOffset(1.4)
      ay.SetTitleOffset(1.8)
  ax.SetLabelSize(labsiz)
  ay.SetLabelSize(labsiz)
  ax.SetTickLength(0.0)
  ay.SetTickLength(0.0)
  az.SetTitle(ztitle)
  az.SetTitleSize(0.04)
  if bigz: az.SetTitleOffset(1.2)

#  h.Draw("colz,text")
  h2= h.Clone(h.GetName()+"_nolim")
  # Work round bug in h.Draw("colz"): it shows white (not yellow) for c<hmin
  for i in range(h.GetSize()):
    c= h.GetBinContent(i)
    if   c<hmin: h.SetBinContent(i,hmin)
    elif c>hmax: h.SetBinContent(i,hmax)
  h.Draw("colz")

  if opt.group:
    line= ROOT.TLine()
    line.SetLineStyle(3)
    tx= NewTLatex(align=21, ndc=False, size=ax.GetLabelSize(), color=ax.GetLabelColor(), font=ax.GetLabelFont())
    ty= NewTLatex(align=21, ndc=False, size=ay.GetLabelSize(), color=ay.GetLabelColor(), font=ay.GetLabelFont())
    ty.SetTextAngle(90)
    if n>16: linex, liney, labpos= -2.0, -1.5, -1.5
    else:    linex, liney, labpos= -1.5, -1.0, -1.1
    lastg=None
    lastlx, lastly= 0, n
    for i,p in enumerate(poiname+[""]):
      if i<n: g= poiGroup(p,model)
      if lastg is not None and (i>=n or g!=lastg):
        lx= ax.GetBinLowEdge(i+1)
        ly= ay.GetBinUpEdge(n-i)
        if i<n:
          line.DrawLine(linex,ly,n,ly)
          line.DrawLine(lx,liney,lx,n)
        px= 0.5*(lx+lastlx)
        py= 0.5*(ly+lastly)
        glatex= niceName(lastg)
        tx.DrawLatex(px,labpos,glatex)
        ty.DrawLatex(labpos,py,glatex)
        lastlx, lastly= lx, ly
      lastg=g

  if   n>40: tsiz= 0.022*20.0/n
  elif n>16: tsiz= 0.016
  else:      tsiz= 0.03
  if verbose>=1: print("tsiz=",tsiz)
  tb= NewTLatex(align=22, ndc=False, color=ROOT.kBlack, size=tsiz)
  tw= NewTLatex(align=22, ndc=False, color=ROOT.kWhite, size=tsiz)
  for ix in range(1,h2.GetNbinsX()+1):
    x= ax.GetBinCenter(ix)
    i= ix-1
    for iy in range(1,h2.GetNbinsY()+1):
      j= n-iy
      y= ay.GetBinCenter(iy)
      c= h2.GetBinContent(ix,iy)
      co= c
      if opt.percent: c *= 100.0
      if blanked[i] or blanked[j]:                                   ct="#minus"
      elif not opt.ratio and not opt.percent and (c==0.0 or c==1.0): ct= "%.0f" % c
      else:
        ct= niceNum(c)
        if opt.variance:
          if ct[0:6]!="#minus": ct= "#kern[1]{"+ct[0]+"}"+ct[1:]
          e= h2.GetBinError(ix,iy)
          if e>=1e-5: ct= "#splitline{"+ct+"}{#pm"+niceNum(e)+"}"
        if verbose>=2: print(x,y,ct)
      if (co-hmin)/(hmax-hmin)<0.9: tb.DrawLatex (x, y, ct)
      else:                        tw.DrawLatex (x, y, ct)

  #if not opt.no_label: #turn off
  t= NewTLatex (size=0.05)
  t.DrawLatex (    canvas.GetLeftMargin(),  0.94, "#bf{#it{ATLAS}}"+(" Preliminary" if opt.preliminary else " Internal" if opt.internal else ""))
  #t.DrawLatex (    canvas.GetLeftMargin(),  0.91, "#bf{#it{ATLAS}} and #bf{#it{CMS}}"+(" Preliminary" if opt.preliminary else " Internal" if opt.internal else ""))
  t.SetTextSize(0.04)
  t.SetTextAlign(31)
  t.DrawLatex (1.0-canvas.GetRightMargin()+0.045, 0.96, ("Expected " if isAsimov else "")+"#it{#sqrt{s}} = 13 TeV, 36.1 - 79.8 fb^{-1}")
  t.DrawLatex (1.0-canvas.GetRightMargin()+0.045, 0.92, "#it{m_{H}} = 125.09 GeV, |#it{y_{H}}| < 2.5")
  #t.DrawLatex (1.0-canvas.GetRightMargin(), 0.91, ("Expected " if isAsimov else "")+"#bf{#it{LHC}} Run 1")

  PlotFile(canvas,output)

# Parse arguments and run main program
exit (process (parseArgs()))

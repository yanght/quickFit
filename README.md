# quickFit
Tool for quickly fitting a data in a RooWorkspace. Please subscribe to quickFit-user@cern.ch for updates or asking questions.

## Installation
To begin with, run
```
git clone ssh://git@gitlab.cern.ch:7999/atlas_higgs_combination/software/quickFit.git
cd quickFit
```

Then please set up ROOT, cmake, and boost. If you are on lxplus, you can directly run
```
source setup_lxplus.sh
```

The quickFit package depends on [RooFitExtensions](https://gitlab.cern.ch/atlas_higgs_combination/software/RooFitExtensions). If the package is not installed yet in your environment, please first run
```
sh scripts/install_roofitext.sh
```
If it is already installed but cmake cannot find the package, please make sure to declare the following environment variable that points to the RooFitExtensions installation path or the folder contains RooFitExtensionsConfig.cmake
```
export RooFitExtensions_DIR=<your path>
```

Now we can proceed to installation. The quickFit package has moved to cmake: **PLEASE NOTE NOW YOU NEED TO RUN MAKE INSTALL**
```
mkdir build 
cd build
cmake ..
make
make install
cd ..
```

On clusters like lxplus where the user does not have the privilege to install software this is generally sufficient. Later every time before using quickFit, one only needs to run the following
```
source setup_lxplus.sh
```
If the user is on a different cluster other than lxplus and does not have cvmfs, one is advised prepare such a setup script following the lxplus example (it is basically setting up ROOT, boost, and cmake, and declare paths to executables and libraries).

If the user has the privilege to install software (e.g. on a docker image or personal computer), one could run
```
make install
```
under "build" directory to copy the executables and libraries to the system paths, such that you do not need to run the setup script in the future.

## Fitting your workspace
Simple fit with MIGRAD
```
quickFit -f filename.root -d dataset -p mu_ggH=1_-5_5,mu_VBF=1_-5_5
```

Fitting `mu_ggH` but fixing `mu_VBF=2`
```
quickFit -f filename.root -d dataset -p mu_ggH=1_-5_5,mu_VBF=2
```

Fixing all systematics with `ATLAS_*` prefix
```
quickFit -f filename.root -d dataset -p mu_ggH=1_-5_5,mu_VBF=1_-5_5 -n ATLAS_*
```

Including HESSE + MINOS fits
```
quickFit -f filename.root -d dataset -p mu_ggH=1_-5_5,mu_VBF=1_-5_5 --hesse 1 --minos 1
```

Outputting nll fit results to `output.root`
```
quickFit -f filename.root -d dataset -p mu_ggH=1_-5_5,mu_VBF=1_-5_5 -o output.root
```
Outputting full fit results to `output.root` (default argument for "-savefitresult" is 1)
```
quickFit -f filename.root -d dataset -p mu_ggH=1_-5_5,mu_VBF=1_-5_5 -o output.root --savefitresult 1
```

# quickAsimov
Tool for creating Asimov dataset in input workspace. The installation is the same as for quickFit.

## How to use it

Prepare an XML file looks like the follwoing:

```
<!DOCTYPE Asimov  SYSTEM 'asimovUtil.dtd'>  
<Asimov InputFile="my_fancy_input_workspace.root" OutputFile="my_fancier_output_workspace.root" POI="mu" >
  <Action Name="Prepare" Setup="" Action="nominalNuis:nominalGlobs"/>
  <Action Name="Fit" Setup="mu=1_0_5" Action="fit:matchglob:savesnapshot" SnapshotNuis="conditionalNuis_1" SnapshotGlob="conditionalGlobs_1"/>
  <Action Name="asimovData_1" Setup="mu=1" Action="genasimov:nominalGlobs"/>
</Asimov>
```
In the first line we provide the input and output path of the workspace we are trying to edit. If you would like to re-define the POI list, you can also list the POIs after the "POI" (otherwise omit the attribute).

The first attribute *"Name"* provides a name for the Asimov dataset if it is to be created (otherwise it does not affect anything in the workspace and is only used for bookkeeping). The second attribute *"Setup"* introduces initial configurations for any parameters in the model (not just POIs). The syntax is exactly the same as for the "-p" option of quickFit.
The third attribute *"Action"* provides a list of actions, indicated by the keywords summarized in the following table, to be performed. The actions are separated by colons.
The forth attribute(s) provide the snapshot names to be saved. One can choose to save snapshots for global observables, nuisance parameters, POIs, or all together.

| *Keyword* |*Meaning*|
|-----------|---------|
| *fit* | Performing a maximum likelihood fit |
| *reset* | Reset all parameters to the states (initial values, ranges, and whether it is floating) before any actions in the current list are taken |
| *raw* | Reset all parameters to the states before any actions (including those in previous lines) are taken |
| *fixsyst* | Fix all the NPs corresponding to constraint pdfs (e.g. NPs for systematic uncertainties) to current values. Unconstrained NPs are not affected |
| *float* | Float all the NPs that have been fixed either in "Setup" or by "fixsyst" keyword. POIs that have been fixed are not affected |
| *genasimov* | Generating Asimov dataset using the name provided with "Name" attribute. *N.B. this keyword can only appear once in each action list* |
| *matchglob* | Matching the global observable values to the corresponding NP ones. *N.B. this keyword should always be accompanied by "reset" at the end of action list* |
| *savesnapshot* | Saving a snapshot, with names provided by the "SnapshotNuis" (for nuisance parameters), "SnapshotGlob" (for global observables), "SnapshotPOI" (for POIs), and/or "SnapshotAll" (for POIs, NPs, and global observables) attributes. *N.B. this keyword can only appear once in each action list* |
| *(snapshot name)* | Load the snapshot with given name |

Coming back to the example, after openning the workspace, we first loaded the nominal nuisance parameter and global observable values using two snapshots "nominalNuis" and "nominalGlobs" (assuming they exist in the input workspace, otherwise we need to create them first using "savesnapshot"), respectively. Then in the second "Action" line we perform a fit with "mu" floating, match the global observable values to the corresponding nuisance parameter ones, and then save snapshots for the best-fit nuisance parameters and global observables (these will be used together with the Asimov). Finally, in the third line, we generate Asimov, and load snapshot "nominalGlobs" to restore the nominal global observable values.

Once the XML card is ready, we can run it with the following command
```
quickAsimov -x (my awesome XML card) -w (workspace name) -m (ModelConfig name) -d (dataset name)
```
Note it is of particular importance to correctly specify the dataset name, such that the program could fit to it.

# quickLimit
Tool for calculating asymptotic CLs limit. The algorithm is from Aaron Armbruster (aaron.james.armbruster@cern.ch).

## How to use it

The syntax is very similiar to quickFit:
```
quickLimit -f filename.root -w workspace -m ModelConfig -d dataset -p mu -o output.root
```
The main differences are the following
   - An root output file has to be provided. It will be used to save the limit results in histograms. A txt file with ".root" replaced with ".txt" saving the limit results will also be produced.
   - The limit program can only process one POI. If "-p" is used, the first POI in the list is used for limit setting. You can still use this argument to configure other POIs. If not provided, the first POI as saved in the ModelConfig will be used.

# quickToy
Tool for generating toy dataset. It will inject a toy dataset into the input workspace based on the snapshots already saved in the workspace. N.B. this tool does not perform any fits. Users need to first run quickAsimov to prepare the snapshots in the workspace before using this tool. Below is an example

```
quickToy -f filename.root -d toyData -p CW=1,CZ=1,Cto=1,Cb=1,Cta=1,Cg=1,Cy=1,BR_Inv=0,BR_Undet=0 -s conditionalGlob_1,conditionalNuis_1 -o output.root --seed 1
```

In this example, we assume that the input workspace already contains conditional fit results for nuisance parameters (conditionalNuis_1) and global observables (conditionalGlob_1) with POIs fixed to SM values.
To be on the safe side, we have also specified the POI values to be on MS using arguments following "-p". The toy dataset to be created has a name of "toyData" and will be saved in the output file "output.root".
"--seed" provide random seed to be used for generating toy dataset. It is very important to use different seed for different toy dataset. Otherwise the toys will be identical.

# quickRebin
This tool can rebin a dataset. This is useful in case the original dataset is too large. Following the example in quickToy, the toy dataset "toyData" created contains binned dataset when histograms are used as PDF,
and unbinned dataset when analytic functions are used. Here we want to convert the unbinned dataset into binned as well to make the fit faster (at the cost of introducing small bias into fit results, which can be mitigated to negligible level by choosing a very fine binning). To do so, run

```
quickRebin -f output.root -d toyData -o output_binned.root -r 500
```

The tool will inject a binned dataset "toyDatabinned" (the binned data name is the input data name + "binned") into the workspace and save it in output file "output_binned.root". The number of bins used is 500 as indicated by "-r".
For categories contains unbinned data but with number of entries less than 500, the dataset will remain unbinned (as introducing binning does not improve fitting speed).

## A note on setup script
The `setup_lxplus.sh` provided in the repository and discussed above is **an example**. It will be maintained up-to-date on a best effort basis, but users are recommended to prepare their own setup script following this example (when using an environment different than `lxplus` this is actually mandatory). 

A different ROOT version can be used as long as it is above 6.18 (please report an issue in case a newer version leads to problems). Cmake and boost versions can also be adjusted.
Essentially you only need to modify the following lines in the script
```
source /cvmfs/sft.cern.ch/lcg/releases/LCG_96b/CMake/3.14.3/x86_64-centos7-gcc8-opt/CMake-env.sh
source /cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/ROOT/v6.20.02/x86_64-centos7-gcc8-opt/ROOT-env.sh
source /cvmfs/sft.cern.ch/lcg/releases/LCG_96b/Boost/1.70.0/x86_64-centos7-gcc8-opt/Boost-env.sh
```
If ROOT, cmake, and boost are already automatically set up, the corresponding lines can be simply removed.

The rest of the script is taking care of the following
* Attach `lib` folder to ${LD_LIBRARY_PATH} (IMPORTANT if you are using any custom class not in vanila ROOT -- otherwise when you open the workspace it will crash)
* Attach `bin` folder to ${PATH} (allow you to use executables everywhere)

##################################################################################################
# Package: quickFit ##############################################################################
cmake_minimum_required( VERSION 3.1 )

# Declare the package name:
project( quickFit )

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})
if(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
  set(CMAKE_INSTALL_PREFIX ${PROJECT_SOURCE_DIR} CACHE PATH "..." FORCE)
endif()
set(CMAKE_INSTALL_LIBDIR ${CMAKE_INSTALL_PREFIX}/lib)

file(MAKE_DIRECTORY ${PROJECT_SOURCE_DIR}/lib)
file(MAKE_DIRECTORY ${PROJECT_SOURCE_DIR}/bin)

# You need to tell CMake where to find the ROOT installation. This can be done in a number of ways:
#   - ROOT built with classic configure/make use the provided $ROOTSYS/etc/cmake/FindROOT.cmake
#   - ROOT built with CMake. Add in CMAKE_PREFIX_PATH the installation prefix for ROOT
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})

# require ROOT
find_package( ROOT REQUIRED COMPONENTS RIO RooFitCore RooFit HistFactory RooStats MathCore MathMore Physics Minuit Foam XMLParser )
include(${ROOT_USE_FILE})

# require Boost
list(APPEND CMAKE_PREFIX_PATH $ENV{BOOST_HOME})
find_package( Boost REQUIRED COMPONENTS program_options )
include_directories(${Boost_INCLUDE_DIRS})

# require RooFitExtensions
find_package(RooFitExtensions REQUIRED CONFIG PATHS ${PROJECT_SOURCE_DIR}/cmake)
include_directories(${RooFitExtensions_INCLUDE_DIRS})
link_directories(${RooFitExtensions_LIBRARY_DIRS})

# Loop over source code
include_directories(${PROJECT_SOURCE_DIR}/inc)

set(quickFitHeaders "")
set(quickFitSources "")

file(GLOB incPaths inc/*)
file(GLOB srcPaths src/*)

foreach (_incPath ${incPaths})
	get_filename_component(_incName ${_incPath} NAME)
    	if(NOT ${_incName} MATCHES "LinkDef.h" AND NOT ${_incName} MATCHES "spdlog")
    	       list(APPEND quickFitHeaders ${_incName})
	endif()
endforeach()

foreach (_srcPath ${srcPaths})
	get_filename_component(_srcName ${_srcPath} NAME)
    	if(NOT ${_srcName} MATCHES "cintdictionary.cxx")
    	       list(APPEND quickFitSources "src/${_srcName}")
	endif()
endforeach()

# generate the dictionary source code
ROOT_GENERATE_DICTIONARY(G__quick ${quickFitHeaders} LINKDEF inc/LinkDef.h)

add_library( quick SHARED ${quickFitSources} G__quick.cxx )

# Build library
target_link_libraries( quick ${ROOT_LIBRARIES} ${RooFitExtensions_LIBRARIES} )

# Build executables

file(GLOB exePaths app/*)

set(quickApp "")

foreach (_exePath ${exePaths})
	get_filename_component(_exeName ${_exePath} NAME_WE)
	get_filename_component(_exeFile ${_exePath} NAME)
	add_executable( ${_exeName} app/${_exeFile} )
	target_link_libraries( ${_exeName} quick Boost::program_options )
	list(APPEND quickApp ${_exeName})
endforeach()

install(TARGETS ${quickApp} DESTINATION bin)
install(TARGETS quick LIBRARY DESTINATION lib)

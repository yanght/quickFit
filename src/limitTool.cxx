#include "limitTool.h"

#include "spdlog/spdlog.h"
//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////

limitTool::limitTool(RooWorkspace *w_, ModelConfig *mc_, RooDataSet *data_, fitTool *fitter, RooRealVar *firstPOI, double initial_guess)
{
  betterBands = 1;           // (recommendation = 1) improve bands by using a more appropriate asimov dataset for those points
  betterNegativeBands = 0;   // (recommendation = 0) improve also the negative bands
  profileNegativeAtZero = 0; // (recommendation = 0) profile asimov for negative bands at zero

  // other configuration
  defaultMinimizer = "Minuit2";        // or "Minuit"
  defaultPrintLevel = -1;              // Minuit print level
  defaultStrategy = 1;                 // Minimization strategy. 0-2. 0 = fastest, least robust. 2 = slowest, most robust
  killBelowFatal = 1;                  // In case you want to suppress RooFit warnings further, set to 1
  doBlind = 0;                         // in case your analysis is blinded
  conditionalExpected = 1 && !doBlind; // Profiling mode for Asimov data: 0 = conditional MLEs, 1 = nominal MLEs
  doTilde = 1;                         // bound mu at zero if true and do the \tilde{q}_{mu} asymptotics. If doTilde = 2, directly bound mu to be positive in unconditional fit
  doExp = 1;                           // compute expected limit
  doObs = 1 && !doBlind;               // compute observed limit
  precision = 0.005;                   // % precision in mu that defines iterative cutoff
  verbose = 0;                         // 1 = very spammy
  usePredictiveFit = 0;                // experimental, extrapolate best fit nuisance parameters based on previous fit results
  extrapolateSigma = 0;                // experimantal, extrapolate sigma based on previous fits
  maxRetries = 3;                      // number of minimize(fcn) retries before giving up

  asimov_0_nll = NULL;
  obs_nll = NULL;
  nrMinimize = 0;
  direction = 1;
  global_status = 0;
  target_CLs = 0.05;

  w = w_;
  mc = mc_;
  data = data_;
  _fitter = fitter;
  _firstPOI = firstPOI;
  _initial_guess = initial_guess;
}

void limitTool::runAsymptoticsCLs(string outputFileName, const char *asimovDataName, double CL)
{
  TStopwatch timer;
  timer.Start();

  // check inputs
  if (!_firstPOI)
  {
    _firstPOI = (RooRealVar *)mc->GetParametersOfInterest()->first();
    spdlog::warn("First POI not specified. Use the default one from ModelConfig {}", _firstPOI->GetName());
  }

  // RooAbsPdf* pdf = mc->GetPdf();
  obs_nll = createNLL(data); //(RooNLLVar*)pdf->createNLL(*data);
  map_snapshots[obs_nll] = "nominalGlobs";
  map_data_nll[data] = obs_nll;
  w->saveSnapshot("nominalGlobs", *mc->GetGlobalObservables());
  w->saveSnapshot("nominalNuis", *mc->GetNuisanceParameters());

  global_status = 0;
  RooDataSet *asimovData_0 = (RooDataSet *)w->data(asimovDataName);
  if (!asimovData_0)
    asimovData_0 = makeAsimovData(conditionalExpected, obs_nll, 0);

  int asimov0_status = global_status;

  asimov_0_nll = createNLL(asimovData_0); //(RooNLLVar*)pdf->createNLL(*asimovData_0);
  map_snapshots[asimov_0_nll] = "conditionalGlobs_0";
  map_data_nll[asimovData_0] = asimov_0_nll;
  setMu(0);
  map_muhat[asimov_0_nll] = 0;
  saveSnapshot(asimov_0_nll, 0);
  w->loadSnapshot("conditionalNuis_0");
  w->loadSnapshot("conditionalGlobs_0");
  map_nll_muhat[asimov_0_nll] = asimov_0_nll->getVal();

  target_CLs = 1 - CL;

  double med_limit = doExp ? getLimit(asimov_0_nll, _initial_guess) : 1.0;
  int med_status = global_status;

  double sigma = med_limit / sqrt(3.84); // pretty close
  double mu_up_p2_approx = sigma * (ROOT::Math::gaussian_quantile(1 - target_CLs * ROOT::Math::gaussian_cdf(2), 1) + 2);
  double mu_up_p1_approx = sigma * (ROOT::Math::gaussian_quantile(1 - target_CLs * ROOT::Math::gaussian_cdf(1), 1) + 1);
  double mu_up_n1_approx = sigma * (ROOT::Math::gaussian_quantile(1 - target_CLs * ROOT::Math::gaussian_cdf(-1), 1) - 1);
  double mu_up_n2_approx = sigma * (ROOT::Math::gaussian_quantile(1 - target_CLs * ROOT::Math::gaussian_cdf(-2), 1) - 2);

  double mu_up_p2 = mu_up_p2_approx;
  double mu_up_p1 = mu_up_p1_approx;
  double mu_up_n1 = mu_up_n1_approx;
  double mu_up_n2 = mu_up_n2_approx;

  if (doTilde == 2)
    _firstPOI->setRange(0, 5 * sigma);
  else
    _firstPOI->setRange(-5 * sigma, 5 * sigma);

  map<int, int> N_status;
  if (betterBands && doExp) // no better time than now to do this
  {
    // find quantiles, starting with +2, since we should be at +1.96 right now

    double init_targetCLs = target_CLs;
    if (doTilde == 2)
      _firstPOI->setRange(0, 5 * sigma);
    else
      _firstPOI->setRange(-5 * sigma, 5 * sigma);
    for (int N = 2; N >= -2; N--)
    {
      if (N < 0 && !betterNegativeBands)
        continue;
      if (N == 0)
        continue;
      target_CLs = 2 * (1 - ROOT::Math::gaussian_cdf(fabs(N))); // change this so findCrossing looks for sqrt(qmu95)=2
      if (N < 0)
        direction = -1;

      // get the acual value
      double NtimesSigma = getLimit(asimov_0_nll, N * med_limit / sqrt(3.84)); // use N * sigma(0) as an initial guess
      N_status[N] += global_status;
      sigma = NtimesSigma / N;
      spdlog::info("Found N * sigma = {} * {}", N, sigma);

      string muStr, muStrPr;
      w->loadSnapshot("conditionalGlobs_0");
      double pr_val = NtimesSigma;
      if (N < 0 && profileNegativeAtZero)
        pr_val = 0;
      RooDataSet *asimovData_N = makeAsimovData(1, asimov_0_nll, NtimesSigma, &muStr, &muStrPr, pr_val, 0);

      RooNLLVar *asimov_N_nll = createNLL(asimovData_N); //(RooNLLVar*)pdf->createNLL(*asimovData_N);
      map_data_nll[asimovData_N] = asimov_N_nll;
      map_snapshots[asimov_N_nll] = "conditionalGlobs" + muStrPr;
      w->loadSnapshot(map_snapshots[asimov_N_nll].c_str());
      w->loadSnapshot(("conditionalNuis" + muStrPr).c_str());
      setMu(NtimesSigma);

      double nll_val = asimov_N_nll->getVal();
      saveSnapshot(asimov_N_nll, NtimesSigma);
      map_muhat[asimov_N_nll] = NtimesSigma;
      if (N < 0 && doTilde > 0)
      {
        setMu(0);
        _firstPOI->setConstant(1);
        nll_val = getNLL(asimov_N_nll);
      }
      map_nll_muhat[asimov_N_nll] = nll_val;

      target_CLs = init_targetCLs;
      direction = 1;
      double initial_guess = findCrossing(NtimesSigma / N, NtimesSigma / N, NtimesSigma);
      double limit = getLimit(asimov_N_nll, initial_guess);
      N_status[N] += global_status;

      if (N == 2)
        mu_up_p2 = limit;
      else if (N == 1)
        mu_up_p1 = limit;
      else if (N == -1)
        mu_up_n1 = limit;
      else if (N == -2)
        mu_up_n2 = limit;
      // return;
    }
    direction = 1;
    target_CLs = init_targetCLs;
  }

  w->loadSnapshot("conditionalNuis_0");
  double obs_limit = doObs ? getLimit(obs_nll, med_limit) : 0;
  int obs_status = global_status;

  bool hasFailures = false;
  if (obs_status != 0 || med_status != 0 || asimov0_status != 0)
    hasFailures = true;
  for (map<int, int>::iterator itr = N_status.begin(); itr != N_status.end(); itr++)
  {
    if (itr->second != 0)
      hasFailures = true;
  }
  if (hasFailures)
  {
    spdlog::error("--------------------------------");
    spdlog::error("Unresolved fit failures detected");
    spdlog::error("Asimov0:  {}", asimov0_status);
    for (map<int, int>::iterator itr = N_status.begin(); itr != N_status.end(); itr++)
    {
      spdlog::error("+{}sigma:  {}", itr->first, itr->second);
    }
    spdlog::error("Median:   {}", med_status);
    spdlog::error("Observed: {}", obs_status);
    spdlog::error("--------------------------------");
  }

  if (doExp && betterBands)
  {
    spdlog::info("Guess for bands");
    spdlog::info("+2sigma: {}", mu_up_p2_approx);
    spdlog::info("+1sigma: {}", mu_up_p1_approx);
    spdlog::info("-1sigma: {}", mu_up_n1_approx);
    spdlog::info("-2sigma: {}", mu_up_n2_approx);
  }

  spdlog::info("--------------------------------");
  spdlog::info("         Final result           ");
  spdlog::info("--------------------------------");

  if (doExp)
  {
    if (betterBands)
    {
      spdlog::info("Correct bands");
      spdlog::info("+2sigma: {}", mu_up_p2);
      spdlog::info("+1sigma: {}", mu_up_p1);
      spdlog::info("-1sigma: {}", mu_up_n1);
      spdlog::info("-2sigma: {}", mu_up_n2);      
    }
    else
    {
      spdlog::info("+2sigma: {}", mu_up_p2_approx);
      spdlog::info("+1sigma: {}", mu_up_p1_approx);
      spdlog::info("-1sigma: {}", mu_up_n1_approx);
      spdlog::info("-2sigma: {}", mu_up_n2_approx);      
    }
  }

  if (doExp)
    spdlog::info("Median:   {}", med_limit);
  if (doObs)
    spdlog::info("Observed: {}", obs_limit);
  spdlog::info("--------------------------------");

  TFile fout(outputFileName.c_str(), "recreate");

  TH1D *h_lim = new TH1D("limit", "limit", 7, 0, 7);
  h_lim->SetBinContent(1, obs_limit);
  h_lim->SetBinContent(2, med_limit);
  h_lim->SetBinContent(3, mu_up_p2);
  h_lim->SetBinContent(4, mu_up_p1);
  h_lim->SetBinContent(5, mu_up_n1);
  h_lim->SetBinContent(6, mu_up_n2);
  h_lim->SetBinContent(7, global_status);

  h_lim->GetXaxis()->SetBinLabel(1, "Observed");
  h_lim->GetXaxis()->SetBinLabel(2, "Expected");
  h_lim->GetXaxis()->SetBinLabel(3, "+2sigma");
  h_lim->GetXaxis()->SetBinLabel(4, "+1sigma");
  h_lim->GetXaxis()->SetBinLabel(5, "-1sigma");
  h_lim->GetXaxis()->SetBinLabel(6, "-2sigma");
  h_lim->GetXaxis()->SetBinLabel(7, "Global status"); // do something with this later

  TH1D *h_lim_old = new TH1D("limit_old", "limit_old", 7, 0, 7); // include also old approximation of bands
  h_lim_old->SetBinContent(1, obs_limit);
  h_lim_old->SetBinContent(2, med_limit);
  h_lim_old->SetBinContent(3, mu_up_p2_approx);
  h_lim_old->SetBinContent(4, mu_up_p1_approx);
  h_lim_old->SetBinContent(5, mu_up_n1_approx);
  h_lim_old->SetBinContent(6, mu_up_n2_approx);
  h_lim_old->SetBinContent(7, global_status);

  h_lim_old->GetXaxis()->SetBinLabel(1, "Observed");
  h_lim_old->GetXaxis()->SetBinLabel(2, "Expected");
  h_lim_old->GetXaxis()->SetBinLabel(3, "+2sigma");
  h_lim_old->GetXaxis()->SetBinLabel(4, "+1sigma");
  h_lim_old->GetXaxis()->SetBinLabel(5, "-1sigma");
  h_lim_old->GetXaxis()->SetBinLabel(6, "-2sigma");
  h_lim_old->GetXaxis()->SetBinLabel(7, "Global status");

  fout.Write();
  fout.Close();

  TString outputTxtFileName = outputFileName;
  outputTxtFileName.ReplaceAll(".root", ".txt");
  ofstream ftxt(outputTxtFileName);
  ftxt << obs_limit << " " << med_limit << " " << mu_up_p2 << " " << mu_up_p1 << " " << mu_up_n1 << " " << mu_up_n2 << endl;
  ftxt.close();

  spdlog::info("Finished with {} calls to minimize (nll)", nrMinimize);
  timer.Print();
}

double limitTool::getLimit(RooNLLVar *nll, double initial_guess)
{
  spdlog::info("------------------------");
  spdlog::info("Getting limit for nll: {}", nll->GetName());
  // get initial guess based on muhat and sigma(muhat)
  _firstPOI->setConstant(0);
  global_status = 0;

  if (nll == asimov_0_nll)
  {
    setMu(0);
    _firstPOI->setConstant(1);
  }

  double muhat;
  if (map_nll_muhat.find(nll) == map_nll_muhat.end())
  {
    double nll_val = getNLL(nll);
    muhat = _firstPOI->getVal();
    saveSnapshot(nll, muhat);
    map_muhat[nll] = muhat;
    if (muhat < 0 && doTilde > 0)
    {
      setMu(0);
      _firstPOI->setConstant(1);
      nll_val = getNLL(nll);
    }

    map_nll_muhat[nll] = nll_val;
  }
  else
  {
    muhat = map_muhat[nll];
  }

  if (muhat < 0.1 || initial_guess != 0)
    setMu(initial_guess);
  double qmu, qmuA;
  double sigma_guess = getSigma(asimov_0_nll, _firstPOI->getVal(), 0, qmu);
  double sigma_b = sigma_guess;
  double mu_guess = findCrossing(sigma_guess, sigma_b, muhat);
  double pmu = calcPmu(qmu, sigma_b, mu_guess);
  double pb = calcPb(qmu, sigma_b, mu_guess);
  double CLs = calcCLs(qmu, sigma_b, mu_guess);
  double qmu95 = getQmu95(sigma_b, mu_guess);
  setMu(mu_guess);

  spdlog::info("Initial guess:  {}", mu_guess);
  spdlog::info("Sigma(obs):     {}", sigma_guess);
  spdlog::info("Sigma(mu,0):    {}", sigma_b);
  spdlog::info("muhat:          {}", muhat);
  spdlog::info("qmu95:          {}", qmu95);
  spdlog::info("qmu:            {}", qmu);
  spdlog::info("pmu:            {}", pmu);
  spdlog::info("1-pb:           {}", pb);
  spdlog::info("CLs:            {}", CLs);

  int nrDamping = 1;
  map<double, double> guess_to_corr;
  double damping_factor = 1.0;
  // double damping_factor_pre = damping_factor;
  int nrItr = 0;
  double mu_pre = muhat; // mu_guess-10*precision*mu_guess;
  double mu_pre2 = muhat;
  while (fabs(mu_pre - mu_guess) > precision * mu_guess * direction)
  {
    spdlog::info("----------------------");
    spdlog::info("Starting iteration {} of {}", nrItr, nll->GetName());
    // do this to avoid comparing multiple minima in the conditional and unconditional fits
    if (nrItr == 0)
      loadSnapshot(nll, muhat);
    else if (usePredictiveFit)
      doPredictiveFit(nll, mu_pre2, mu_pre, mu_guess);
    else
      loadSnapshot(asimov_0_nll, mu_pre);

    sigma_guess = getSigma(nll, mu_guess, muhat, qmu);
    saveSnapshot(nll, mu_guess);

    if (nll != asimov_0_nll)
    {
      if (nrItr == 0)
        loadSnapshot(asimov_0_nll, map_nll_muhat[asimov_0_nll]);
      else if (usePredictiveFit)
      {
        if (nrItr == 1)
          doPredictiveFit(nll, map_nll_muhat[asimov_0_nll], mu_pre, mu_guess);
        else
          doPredictiveFit(nll, mu_pre2, mu_pre, mu_guess);
      }
      else
        loadSnapshot(asimov_0_nll, mu_pre);

      sigma_b = getSigma(asimov_0_nll, mu_guess, 0, qmuA);
      saveSnapshot(asimov_0_nll, mu_guess);
    }
    else
    {
      sigma_b = sigma_guess;
      qmuA = qmu;
    }

    double corr = damping_factor * (mu_guess - findCrossing(sigma_guess, sigma_b, muhat));
    for (map<double, double>::iterator itr = guess_to_corr.begin(); itr != guess_to_corr.end(); itr++)
    {
      if (fabs(itr->first - (mu_guess - corr)) < direction * mu_guess * 0.02 && fabs(corr) > direction * mu_guess * precision)
      {
        damping_factor *= 0.8;
        spdlog::info("Changing damping factor to {}, nrDamping = {}", damping_factor, nrDamping);
        if (nrDamping++ > 10)
        {
          nrDamping = 1;
          damping_factor = 1.0;
        }
        corr *= damping_factor;
        break;
      }
    }

    // subtract off the difference in the new and damped correction
    guess_to_corr[mu_guess] = corr;
    mu_pre2 = mu_pre;
    mu_pre = mu_guess;
    mu_guess -= corr;

    pmu = calcPmu(qmu, sigma_b, mu_pre);
    pb = calcPb(qmu, sigma_b, mu_pre);
    CLs = calcCLs(qmu, sigma_b, mu_pre);
    qmu95 = getQmu95(sigma_b, mu_pre);

    spdlog::info("NLL:            {}", nll->GetName());
    spdlog::info("Previous guess: {}", mu_pre);
    spdlog::info("Sigma(obs):     {}", sigma_guess);
    spdlog::info("Sigma(mu,0):    {}", sigma_b);
    spdlog::info("muhat:          {}", muhat);
    spdlog::info("pmu:            {}", pmu);
    spdlog::info("1-pb:           {}", pb);
    spdlog::info("CLs:            {}", CLs);
    spdlog::info("qmu95:          {}", qmu95);
    spdlog::info("qmu:            {}", qmu);
    spdlog::info("qmuA0:          {}", qmuA);
    spdlog::info("Precision:      {}", direction * mu_guess * precision);
    spdlog::info("Correction:     {}", -corr);
    spdlog::info("New guess:      {}", mu_guess);

    nrItr++;
    if (nrItr > 25)
    {
      spdlog::error("Infinite loop detected in getLimit(). Please intervene...");
      break;
    }
  }

  spdlog::info("Found limit for nll {}: {}", nll->GetName(), mu_guess);
  spdlog::info("Finished in {} iterations.", nrItr);
  return mu_guess;
}

double limitTool::getSigma(RooNLLVar *nll, double mu, double muhat, double &qmu)
{
  qmu = getQmu(nll, mu);
  if (verbose)
    spdlog::info("qmu = {}", qmu);
  if (mu * direction < muhat)
    return fabs(mu - muhat) / sqrt(qmu);
  else if (muhat < 0 && doTilde > 0)
    return sqrt(mu * mu - 2 * mu * muhat * direction) / sqrt(qmu);
  else
    return (mu - muhat) * direction / sqrt(qmu);
}

double limitTool::getQmu(RooNLLVar *nll, double mu)
{
  double nll_muhat = map_nll_muhat[nll];
  bool isConst = _firstPOI->isConstant();
  _firstPOI->setConstant(1);
  setMu(mu);
  double nll_val = getNLL(nll);
  _firstPOI->setConstant(isConst);

  return 2 * (nll_val - nll_muhat);
}

void limitTool::saveSnapshot(RooNLLVar *nll, double mu)
{
  stringstream snapshotName;
  snapshotName << nll->GetName() << "_" << mu;
  w->saveSnapshot(snapshotName.str().c_str(), *mc->GetNuisanceParameters());
}

void limitTool::loadSnapshot(RooNLLVar *nll, double mu)
{
  stringstream snapshotName;
  snapshotName << nll->GetName() << "_" << mu;
  w->loadSnapshot(snapshotName.str().c_str());
}

void limitTool::doPredictiveFit(RooNLLVar *nll, double mu1, double mu2, double mu)
{
  if (fabs(mu2 - mu) < direction * mu * precision * 4)
  {
    loadSnapshot(nll, mu2);
    return;
  }

  // extrapolate to mu using mu1 and mu2 assuming nuis scale linear in mu
  const RooArgSet *nuis = mc->GetNuisanceParameters();
  int nrNuis = nuis->getSize();
  double *theta_mu1 = new double[nrNuis];
  double *theta_mu2 = new double[nrNuis];

  TIterator *itr = nuis->createIterator();
  RooRealVar *var;
  int counter = 0;
  loadSnapshot(nll, mu1);
  while ((var = (RooRealVar *)itr->Next()))
  {
    theta_mu1[counter++] = var->getVal();
  }

  itr->Reset();
  counter = 0;
  loadSnapshot(nll, mu2);
  while ((var = (RooRealVar *)itr->Next()))
  {
    theta_mu2[counter++] = var->getVal();
  }

  itr->Reset();
  counter = 0;
  while ((var = (RooRealVar *)itr->Next()))
  {
    double m = (theta_mu2[counter] - theta_mu1[counter]) / (mu2 - mu1);
    double b = theta_mu2[counter] - m * mu2;
    double theta_extrap = m * mu + b;

    var->setVal(theta_extrap);
    counter++;
  }

  delete itr;
  delete[] theta_mu1;
  delete[] theta_mu2;
}

RooNLLVar *limitTool::createNLL(RooDataSet *_data)
{
  // RooArgSet nuis = *mc->GetNuisanceParameters();
  // RooNLLVar* nll = (RooNLLVar*)mc->GetPdf()->createNLL(*_data, Constrain(nuis));
  // return nll;
  return (RooNLLVar *)_fitter->createNLL(mc, _data);
}

double limitTool::getNLL(RooNLLVar *nll)
{
  string snapshotName = map_snapshots[nll];
  if (snapshotName != "")
    w->loadSnapshot(snapshotName.c_str());
  minimize(nll);
  double val = nll->getVal();
  w->loadSnapshot("nominalGlobs");
  return val;
}

double limitTool::findCrossing(double sigma_obs, double sigma, double muhat)
{
  double mu_guess = muhat + ROOT::Math::gaussian_quantile(1 - target_CLs, 1) * sigma_obs * direction;
  int nrItr = 0;
  int nrDamping = 1;

  map<double, double> guess_to_corr;
  double damping_factor = 1.0;
  double mu_pre = mu_guess - 10 * mu_guess * precision;
  while (fabs(mu_guess - mu_pre) > direction * mu_guess * precision)
  {
    double sigma_obs_extrap = sigma_obs;
    double eps = 0;
    if (extrapolateSigma)
    {
      // map<double, double> map_mu_sigma = map_nll_mu_sigma[nll];
    }

    mu_pre = mu_guess;

    double qmu95 = getQmu95(sigma, mu_guess);
    double qmu;
    qmu = 1. / sigma_obs_extrap / sigma_obs_extrap * (mu_guess - muhat) * (mu_guess - muhat);
    if (muhat < 0 && doTilde > 0)
      qmu = 1. / sigma_obs_extrap / sigma_obs_extrap * (mu_guess * mu_guess - 2 * mu_guess * muhat);

    double dqmu_dmu = 2 * (mu_guess - muhat) / sigma_obs_extrap / sigma_obs_extrap - 2 * qmu * eps;

    double corr = damping_factor * (qmu - qmu95) / dqmu_dmu;
    for (map<double, double>::iterator itr = guess_to_corr.begin(); itr != guess_to_corr.end(); itr++)
    {
      if (fabs(itr->first - mu_guess) < direction * mu_guess * precision)
      {
        damping_factor *= 0.8;
        if (verbose)
          spdlog::info("Changing damping factor to {}, nrDamping = {}", damping_factor, nrDamping);
        if (nrDamping++ > 10)
        {
          nrDamping = 1;
          damping_factor = 1.0;
        }
        corr *= damping_factor;
        break;
      }
    }
    guess_to_corr[mu_guess] = corr;

    mu_guess = mu_guess - corr;
    nrItr++;
    if (nrItr > 100)
    {
      spdlog::error("Infinite loop detected in findCrossing. Please intervene...");
      exit(1);
    }
    if (verbose)
      spdlog::info("mu_guess = {}, mu_pre = {}, qmu = {}, qmu95 = {}, sigma_obs_extrap = {}, sigma = {}, direction*mu*prec = {}", mu_guess, mu_pre, qmu, qmu95, sigma_obs_extrap, sigma, direction * mu_guess * precision);
  }

  return mu_guess;
}

void limitTool::setMu(double mu)
{
  if (mu != mu)
  {
    spdlog::error("POI becomes NaN. Please intervene...");
    exit(1);
  }
  if (mu > 0 && _firstPOI->getMax() < mu)
    _firstPOI->setMax(2 * mu);
  if (mu < 0 && _firstPOI->getMin() > mu)
    _firstPOI->setMin(2 * mu);
  _firstPOI->setVal(mu);
}

double limitTool::getQmu95_brute(double sigma, double mu)
{
  double step_size = 0.001;
  double start = step_size;
  if (mu / sigma > 0.2)
    start = 0;
  for (double qmu = start; qmu < 20; qmu += step_size)
  {
    double CLs = calcCLs(qmu, sigma, mu);

    if (CLs < target_CLs)
      return qmu;
  }

  return 20;
}

double limitTool::getQmu95(double sigma, double mu)
{
  double qmu95 = 0;
  // no sane man would venture this far down into |mu/sigma|
  double target_N = ROOT::Math::gaussian_quantile(1 - target_CLs, 1);
  if (fabs(mu / sigma) < 0.25 * target_N)
  {
    qmu95 = 5.83 / target_N;
  }
  else
  {
    map<double, double> guess_to_corr;
    double qmu95_guess = pow(ROOT::Math::gaussian_quantile(1 - target_CLs, 1), 2);
    int nrItr = 0;
    int nrDamping = 1;
    double damping_factor = 1.0;
    double qmu95_pre = qmu95_guess - 10 * 2 * qmu95_guess * precision;
    while (fabs(qmu95_guess - qmu95_pre) > 2 * qmu95_guess * precision)
    {
      qmu95_pre = qmu95_guess;
      if (verbose)
      {
        spdlog::info("qmu95_guess = {}", qmu95_guess);
        spdlog::info("CLs = {}", calcCLs(qmu95_guess, sigma, mu));
        spdlog::info("Derivative = {}", calcDerCLs(qmu95_guess, sigma, mu));
      }

      double corr = damping_factor * (calcCLs(qmu95_guess, sigma, mu) - target_CLs) / calcDerCLs(qmu95_guess, sigma, mu);
      for (map<double, double>::iterator itr = guess_to_corr.begin(); itr != guess_to_corr.end(); itr++)
      {
        if (fabs(itr->first - qmu95_guess) < 2 * qmu95_guess * precision)
        {
          damping_factor *= 0.8;
          if (verbose)
            spdlog::info("Changing damping factor to {}, nrDamping = {}", damping_factor, nrDamping);
          if (nrDamping++ > 10)
          {
            nrDamping = 1;
            damping_factor = 1.0;
          }
          corr *= damping_factor;
        }
      }

      guess_to_corr[qmu95_guess] = corr;
      qmu95_guess = qmu95_guess - corr;

      if (verbose)
      {
        spdlog::info("next guess = {}", qmu95_guess);
        spdlog::info("precision = {}", 2 * qmu95_guess * precision);
      }
      nrItr++;
      if (nrItr > 200)
      {
        spdlog::error("Infinite loop detected in getQmu95. Please intervene...");
        exit(1);
      }
    }
    qmu95 = qmu95_guess;
  }

  if (qmu95 != qmu95)
  {
    qmu95 = getQmu95_brute(sigma, mu);
  }
  if (verbose)
    spdlog::info("Returning qmu95 = {}", qmu95);

  return qmu95;
}

double limitTool::calcCLs(double qmu_tilde, double sigma, double mu)
{
  double pmu = calcPmu(qmu_tilde, sigma, mu);
  double pb = calcPb(qmu_tilde, sigma, mu);
  if (verbose)
  {
    spdlog::info("pmu = ", pmu);
    spdlog::info("pb = ", pb);
  }
  if (pb == 1)
    return 0.5;
  return pmu / (1 - pb);
}

double limitTool::calcPmu(double qmu, double sigma, double mu)
{
  double pmu;
  if (qmu < mu * mu / (sigma * sigma) || !(doTilde > 0))
  {
    pmu = 1 - ROOT::Math::gaussian_cdf(sqrt(qmu));
  }
  else
  {
    pmu = 1 - ROOT::Math::gaussian_cdf((qmu + mu * mu / (sigma * sigma)) / (2 * fabs(mu / sigma)));
  }
  if (verbose)
    spdlog::info("for pmu, qmu = {}, sigma = {}, mu = {}, pmu = {}", qmu, sigma, mu, pmu);
  return pmu;
}

double limitTool::calcPb(double qmu, double sigma, double mu)
{
  if (qmu < mu * mu / (sigma * sigma) || !(doTilde > 0))
  {
    return 1 - ROOT::Math::gaussian_cdf(fabs(mu / sigma) - sqrt(qmu));
  }
  else
  {
    return 1 - ROOT::Math::gaussian_cdf((mu * mu / (sigma * sigma) - qmu) / (2 * fabs(mu / sigma)));
  }
}

double limitTool::calcDerCLs(double qmu, double sigma, double mu)
{
  double dpmu_dq = 0;
  double d1mpb_dq = 0;

  if (qmu < mu * mu / (sigma * sigma))
  {
    double zmu = sqrt(qmu);
    dpmu_dq = -1. / (2 * sqrt(qmu * 2 * TMath::Pi())) * exp(-zmu * zmu / 2);
  }
  else
  {
    double zmu = (qmu + mu * mu / (sigma * sigma)) / (2 * fabs(mu / sigma));
    dpmu_dq = -1. / (2 * fabs(mu / sigma)) * 1. / (sqrt(2 * TMath::Pi())) * exp(-zmu * zmu / 2);
  }

  if (qmu < mu * mu / (sigma * sigma))
  {
    double zb = fabs(mu / sigma) - sqrt(qmu);
    d1mpb_dq = -1. / sqrt(qmu * 2 * TMath::Pi()) * exp(-zb * zb / 2) / 2.;
  }
  else
  {
    double zb = (mu * mu / (sigma * sigma) - qmu) / (2 * fabs(mu / sigma));
    d1mpb_dq = -1. / (2 * fabs(mu / sigma)) * 1. / (sqrt(2 * TMath::Pi())) * exp(-zb * zb / 2);
  }

  double pb = calcPb(qmu, sigma, mu);
  return dpmu_dq / (1 - pb) - calcCLs(qmu, sigma, mu) / (1 - pb) * d1mpb_dq;
}

int limitTool::minimize(RooNLLVar *nll)
{
  nrMinimize++;
  RooAbsReal *fcn = (RooAbsReal *)nll;
  _fitter->minimize(fcn, mc);
  return _fitter->status();
}

void limitTool::unfoldConstraints(RooArgSet &initial, RooArgSet &final, RooArgSet &obs, RooArgSet &nuis, int &counter)
{
  if (counter > 50)
  {
    spdlog::error("ERROR::Couldn't unfold constraints!");
    spdlog::error("Initial:");
    initial.Print("v");
    spdlog::error("Final:");
    final.Print("v");
    exit(1);
  }
  TIterator *itr = initial.createIterator();
  RooAbsPdf *pdf;
  while ((pdf = (RooAbsPdf *)itr->Next()))
  {
    RooArgSet nuis_tmp = nuis;
    RooArgSet constraint_set(*pdf->getAllConstraints(obs, nuis_tmp, false));
    // if (constraint_set.getSize() > 1)
    //{
    string className(pdf->ClassName());
    if (className != "RooGaussian" && className != "RooLognormal" && className != "RooGamma" && className != "RooPoisson" && className != "RooBifurGauss")
    {
      counter++;
      unfoldConstraints(constraint_set, final, obs, nuis, counter);
    }
    else
    {
      final.add(*pdf);
    }
  }
  delete itr;
}

RooDataSet *limitTool::makeAsimovData(bool doConditional, RooNLLVar *conditioning_nll, double mu_val, string *mu_str, string *mu_prof_str, double mu_val_profile, bool doFit)
{
  if (mu_val_profile == -999)
    mu_val_profile = mu_val;

  spdlog::info("Creating asimov data at mu = {}, profiling at mu = {}", mu_val, mu_val_profile);

  ////////////////////
  // make asimov data//
  ////////////////////
  RooAbsPdf *combPdf = mc->GetPdf();

  int _printLevel = 0;

  stringstream muStr;
  muStr << setprecision(5);
  muStr << "_" << mu_val;
  if (mu_str)
    *mu_str = muStr.str();

  stringstream muStrProf;
  muStrProf << setprecision(5);
  muStrProf << "_" << mu_val_profile;
  if (mu_prof_str)
    *mu_prof_str = muStrProf.str();

  RooRealVar *mu = _firstPOI;
  mu->setVal(mu_val);

  RooArgSet mc_obs = *mc->GetObservables();
  RooArgSet mc_globs = *mc->GetGlobalObservables();
  RooArgSet mc_nuis = *mc->GetNuisanceParameters();

  // pair the nuisance parameter to the global observable
  RooArgSet mc_nuis_tmp = mc_nuis;
  RooArgList nui_list("ordered_nuis");
  RooArgList glob_list("ordered_globs");
  RooArgSet constraint_set_tmp(*combPdf->getAllConstraints(mc_obs, mc_nuis_tmp, false));
  RooArgSet constraint_set;
  int counter_tmp = 0;
  unfoldConstraints(constraint_set_tmp, constraint_set, mc_obs, mc_nuis_tmp, counter_tmp);

  TIterator *cIter = constraint_set.createIterator();
  RooAbsArg *arg;
  while ((arg = (RooAbsArg *)cIter->Next()))
  {
    RooAbsPdf *pdf = (RooAbsPdf *)arg;
    if (!pdf)
      continue;

    TIterator *nIter = mc_nuis.createIterator();
    RooRealVar *thisNui = NULL;
    RooAbsArg *nui_arg;
    while ((nui_arg = (RooAbsArg *)nIter->Next()))
    {
      if (pdf->dependsOn(*nui_arg))
      {
        thisNui = (RooRealVar *)nui_arg;
        break;
      }
    }
    delete nIter;

    // RooRealVar* thisNui = (RooRealVar*)pdf->getObservables();

    // need this incase the observable isn't fundamental.
    // in this case, see which variable is dependent on the nuisance parameter and use that.
    RooArgSet *components = pdf->getComponents();

    components->remove(*pdf);
    if (components->getSize())
    {
      TIterator *itr1 = components->createIterator();
      RooAbsArg *arg1;
      while ((arg1 = (RooAbsArg *)itr1->Next()))
      {
        TIterator *itr2 = components->createIterator();
        RooAbsArg *arg2;
        while ((arg2 = (RooAbsArg *)itr2->Next()))
        {
          if (arg1 == arg2)
            continue;
          if (arg2->dependsOn(*arg1))
          {
            components->remove(*arg1);
          }
        }
        delete itr2;
      }
      delete itr1;
    }
    if (components->getSize() > 1)
    {
      spdlog::error("Couldn't isolate proper nuisance parameter");
      return NULL;
    }
    else if (components->getSize() == 1)
    {
      thisNui = (RooRealVar *)components->first();
    }

    TIterator *gIter = mc_globs.createIterator();
    RooRealVar *thisGlob = NULL;
    RooAbsArg *glob_arg;
    while ((glob_arg = (RooAbsArg *)gIter->Next()))
    {
      if (pdf->dependsOn(*glob_arg))
      {
        thisGlob = (RooRealVar *)glob_arg;
        break;
      }
    }
    delete gIter;

    if (!thisNui || !thisGlob)
    {
      spdlog::warn("Couldn't find nui or glob for constraint: {}", pdf->GetName());
      // return;
      continue;
    }

    if (_printLevel >= 1)
      spdlog::info("Pairing nui: {}, with glob: {}, from constraint: {}", thisNui->GetName(), thisGlob->GetName(), pdf->GetName());

    nui_list.add(*thisNui);
    glob_list.add(*thisGlob);
  }
  delete cIter;

  // save the snapshots of nominal parameters, but only if they're not already saved
  w->saveSnapshot("tmpGlobs", *mc->GetGlobalObservables());
  w->saveSnapshot("tmpNuis", *mc->GetNuisanceParameters());
  if (!w->loadSnapshot("nominalGlobs"))
  {
    spdlog::warn("nominalGlobs doesn't exist. Saving snapshot...");
    w->saveSnapshot("nominalGlobs", *mc->GetGlobalObservables());
  }
  else
    w->loadSnapshot("tmpGlobs");
  if (!w->loadSnapshot("nominalNuis"))
  {
    spdlog::warn("nominalNuis doesn't exist. Saving snapshot...");
    w->saveSnapshot("nominalNuis", *mc->GetNuisanceParameters());
  }
  else
    w->loadSnapshot("tmpNuis");

  RooArgSet nuiSet_tmp(nui_list);

  mu->setVal(mu_val_profile);
  mu->setConstant(1);
  // int status = 0;
  if (doConditional && doFit)
    minimize(conditioning_nll);

  mu->setConstant(0);
  mu->setVal(mu_val);

  // loop over the nui/glob list, grab the corresponding variable from the tmp ws, and set the glob to the value of the nui
  int nrNuis = nui_list.getSize();
  if (nrNuis != glob_list.getSize())
  {
    spdlog::error("nui_list.getSize() != glob_list.getSize()!");
    return NULL;
  }

  for (int i = 0; i < nrNuis; i++)
  {
    RooRealVar *nui = (RooRealVar *)nui_list.at(i);
    RooRealVar *glob = (RooRealVar *)glob_list.at(i);

    glob->setVal(nui->getVal());
  }

  // save the snapshots of conditional parameters
  w->saveSnapshot(("conditionalGlobs" + muStrProf.str()).c_str(), *mc->GetGlobalObservables());
  w->saveSnapshot(("conditionalNuis" + muStrProf.str()).c_str(), *mc->GetNuisanceParameters());

  if (!doConditional)
  {
    w->loadSnapshot("nominalGlobs");
    w->loadSnapshot("nominalNuis");
  }

  if (_printLevel >= 1)
    spdlog::info("Making asimov");
  // make the asimov data (snipped from Kyle)
  mu->setVal(mu_val);

  int iFrame = 0;

  const char *weightName = "weightVar";
  RooArgSet obsAndWeight;
  obsAndWeight.add(*mc->GetObservables());

  RooRealVar *weightVar = NULL;
  if (!(weightVar = w->var(weightName)))
  {
    w->import(*(new RooRealVar(weightName, weightName, 1)));
    weightVar = w->var(weightName);
  }
  obsAndWeight.add(*w->var(weightName));

  w->defineSet("obsAndWeight", obsAndWeight);

  //////////////////////////////////////////////////////
  //////////////////////////////////////////////////////
  //////////////////////////////////////////////////////
  //////////////////////////////////////////////////////
  //////////////////////////////////////////////////////
  // MAKE ASIMOV DATA FOR OBSERVABLES

  RooSimultaneous *simPdf = dynamic_cast<RooSimultaneous *>(mc->GetPdf());

  RooDataSet *asimovData;
  if (!simPdf)
  {
    // Get pdf associated with state from simpdf
    RooAbsPdf *pdftmp = mc->GetPdf(); // simPdf->getPdf(channelCat->getLabel()) ;

    // Generate observables defined by the pdf associated with this state
    RooArgSet *obstmp = pdftmp->getObservables(*mc->GetObservables());

    if (_printLevel >= 1)
    {
      obstmp->Print();
    }

    asimovData = new RooDataSet(("asimovData" + muStr.str()).c_str(), ("asimovData" + muStr.str()).c_str(), RooArgSet(obsAndWeight), WeightVar(*weightVar));

    RooRealVar *thisObs = ((RooRealVar *)obstmp->first());
    double expectedEvents = pdftmp->expectedEvents(*obstmp);
    double thisNorm = 0;
    for (int jj = 0; jj < thisObs->numBins(); ++jj)
    {
      thisObs->setBin(jj);

      thisNorm = pdftmp->getVal(obstmp) * thisObs->getBinWidth(jj);
      if (thisNorm * expectedEvents <= 0)
      {
        spdlog::warn("Detected bin with zero expected events ({}) ! Please check your inputs. Obs = {}, bin = {}", thisNorm * expectedEvents, thisObs->GetName(), jj);
      }
      if (thisNorm * expectedEvents > 0 && thisNorm * expectedEvents < pow(10.0, 18))
        asimovData->add(*mc->GetObservables(), thisNorm * expectedEvents);
    }

    if (_printLevel >= 1)
    {
      asimovData->Print();
      spdlog::info("Sum entries {}", asimovData->sumEntries());
    }
    if (asimovData->sumEntries() != asimovData->sumEntries())
    {
      spdlog::error("Sum entries is NaN");
      exit(1);
    }

    w->import(*asimovData);

    if (_printLevel >= 1)
      asimovData->Print();
  }
  else
  {
    map<string, RooDataSet *> asimovDataMap;

    // try fix for sim pdf
    RooCategory *channelCat = (RooCategory *)&simPdf->indexCat(); //(RooCategory*)w->cat("master_channel");//(RooCategory*) (&simPdf->indexCat());
    //    TIterator* iter = simPdf->indexCat().typeIterator() ;
    TIterator *iter = channelCat->typeIterator();
    RooCatType *tt = NULL;
    int nrIndices = 0;
    while ((tt = (RooCatType *)iter->Next()))
    {
      nrIndices++;
    }
    for (int i = 0; i < nrIndices; i++)
    {
      channelCat->setIndex(i);
      iFrame++;
      // Get pdf associated with state from simpdf
      RooAbsPdf *pdftmp = simPdf->getPdf(channelCat->getLabel());

      // Generate observables defined by the pdf associated with this state
      RooArgSet *obstmp = pdftmp->getObservables(*mc->GetObservables());

      if (_printLevel >= 1)
      {
        obstmp->Print();
        spdlog::info("On type {} {}", channelCat->getLabel(), iFrame);
      }

      RooDataSet *obsDataUnbinned = new RooDataSet(Form("combAsimovData%d", iFrame), Form("combAsimovData%d", iFrame), RooArgSet(obsAndWeight, *channelCat), WeightVar(*weightVar));
      RooRealVar *thisObs = ((RooRealVar *)obstmp->first());
      double expectedEvents = pdftmp->expectedEvents(*obstmp);
      double thisNorm = 0;
      for (int jj = 0; jj < thisObs->numBins(); ++jj)
      {
        thisObs->setBin(jj);

        thisNorm = pdftmp->getVal(obstmp) * thisObs->getBinWidth(jj);
        if (thisNorm * expectedEvents > 0 && thisNorm * expectedEvents < pow(10.0, 18))
          obsDataUnbinned->add(*mc->GetObservables(), thisNorm * expectedEvents);
      }

      if (_printLevel >= 1)
      {
        obsDataUnbinned->Print();
        spdlog::info("Sum entries {}", obsDataUnbinned->sumEntries());
      }
      if (obsDataUnbinned->sumEntries() != obsDataUnbinned->sumEntries())
      {
        spdlog::error("Sum entries is NaN");
        exit(1);
      }

      asimovDataMap[string(channelCat->getLabel())] = obsDataUnbinned; // tempData;

      if (_printLevel >= 1)
      {
        spdlog::info("Channel: {}, data:", channelCat->getLabel());
        obsDataUnbinned->Print();
      }
    }

    asimovData = new RooDataSet(("asimovData" + muStr.str()).c_str(), ("asimovData" + muStr.str()).c_str(), RooArgSet(obsAndWeight, *channelCat), Index(*channelCat), Import(asimovDataMap), WeightVar(*weightVar));
    w->import(*asimovData);
  }

  // bring us back to nominal for exporting
  // w->loadSnapshot("nominalNuis");
  w->loadSnapshot("nominalGlobs");

  return asimovData;
}

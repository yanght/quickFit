#include "fitTool.h"

#include "spdlog/spdlog.h"

fitTool::fitTool()
{
  // Default values for the minimizer
  _minAlgo = "Minuit2";
  _outputFile = "";
  _ssName = "ucmles";
  _rangeName = "";
  _minTolerance = 1E-03;
  _minStrat = 1;
  _nllOffset = true;
  _nCPU = 1;
  _optConst = 2;
  _printLevel = 2;
  _useHESSE = false;
  _useMINOS = 0;
  _useSIMPLEX = false;
  _fixStarCache = false;
  _saveWS = false;
  _externalConstraint = NULL;
  _paramList = NULL;
  _saveErrors = false;
  _saveFitResult = false;
  _status = -1;
  _GKIntegrator = false;
  _saveNP = false;
  _errorLevel = -1;
  _doSumW2 = false;
  _useBatch = false;
  _samplingRelTol = -1.;
  _saveHesse = false;
  _fitHesse = false;
}

bool fitTool::checkModel(const RooStats::ModelConfig &model, bool throwOnFail)
{
  // -----------------------------------------------------------------------------------------------------
  bool ok = true;
  std::ostringstream errors;
  std::unique_ptr<TIterator> iter;
  RooAbsPdf *pdf = model.GetPdf();
  if (pdf == 0)
    throw std::invalid_argument("Model without Pdf");

  RooArgSet allowedToFloat;

  // Check model observables
  if (model.GetObservables() == 0)
  {
    ok = false;
    errors << "ERROR: model does not define observables.\n";
    spdlog::error(errors.str());
    if (throwOnFail)
      throw std::invalid_argument(errors.str());
    else
      return false;
  }
  else
  {
    allowedToFloat.add(*model.GetObservables());
  }

  // Check model parameters of interset
  if (model.GetParametersOfInterest() == 0)
  {
    ok = false;
    errors << "ERROR: model does not define parameters of interest.\n";
  }
  else
  {
    iter.reset(model.GetParametersOfInterest()->createIterator());
    for (RooAbsArg *a = (RooAbsArg *)iter->Next(); a != 0; a = (RooAbsArg *)iter->Next())
    {
      RooRealVar *v = dynamic_cast<RooRealVar *>(a);
      if (!v)
      {
        errors << "ERROR: parameter of interest " << a->GetName() << " is a " << a->ClassName() << " and not a RooRealVar\n";
        ok = false;
        continue;
      }
      if (!pdf->dependsOn(*v))
      {
        errors << "ERROR: pdf does not depend on parameter of interest " << a->GetName() << "\n";
        ok = false;
        continue;
      }
      allowedToFloat.add(*v);
    }
  }

  // Check model nuisance parameters
  if (model.GetNuisanceParameters() != 0)
  {
    iter.reset(model.GetNuisanceParameters()->createIterator());
    for (RooAbsArg *a = (RooAbsArg *)iter->Next(); a != 0; a = (RooAbsArg *)iter->Next())
    {
      RooRealVar *v = dynamic_cast<RooRealVar *>(a);
      if (!v)
      {
        errors << "ERROR: nuisance parameter " << a->GetName() << " is a " << a->ClassName() << " and not a RooRealVar\n";
        ok = false;
        continue;
      }
      if (v->isConstant())
      {
        errors << "ERROR: nuisance parameter " << a->GetName() << " is constant\n";
        ok = false;
        continue;
      }
      if (!pdf->dependsOn(*v))
      {
        errors << "WARNING: pdf does not depend on nuisance parameter, removing " << a->GetName() << "\n";
        const_cast<RooArgSet *>(model.GetNuisanceParameters())->remove(*a);
        continue;
      }
      allowedToFloat.add(*v);
    }
  }

  // check model global observables
  if (model.GetGlobalObservables() != 0)
  {
    iter.reset(model.GetGlobalObservables()->createIterator());
    for (RooAbsArg *a = (RooAbsArg *)iter->Next(); a != 0; a = (RooAbsArg *)iter->Next())
    {
      RooRealVar *v = dynamic_cast<RooRealVar *>(a);
      if (!v)
      {
        ok = false;
        errors << "ERROR: global observable " << a->GetName() << " is a " << a->ClassName() << " and not a RooRealVar\n";
        continue;
      }
      if (!v->isConstant())
      {
        ok = false;
        errors << "ERROR: global observable " << a->GetName() << " is not constant\n";
        continue;
      }
      if (!pdf->dependsOn(*v))
      {
        errors << "WARNING: pdf does not depend on global observable " << a->GetName() << "\n";
        continue;
      }
    }
  }

  // check the rest of the pdf
  std::unique_ptr<RooArgSet> params(pdf->getParameters(*model.GetObservables()));
  iter.reset(params->createIterator());
  for (RooAbsArg *a = (RooAbsArg *)iter->Next(); a != 0; a = (RooAbsArg *)iter->Next())
  {
    if (a->isConstant() || allowedToFloat.contains(*a))
      continue;
    if (a->getAttribute("flatParam"))
      continue;
    errors << "WARNING: pdf parameter " << a->GetName() << " (type " << a->ClassName() << ")"
           << " is not allowed to float (it's not nuisance, poi, observable or global observable)\n";
  }
  iter.reset();
  if (errors.str() != "")
    spdlog::error(errors.str());
  if (!ok && throwOnFail)
    throw std::invalid_argument(errors.str());
  return ok;
}

int fitTool::profileToData(ModelConfig *mc, RooAbsData *data)
{
  // Optimize intial values of parameters using saved Hesse pdf
  if (_fitHesse)
  {
    TString hessePdfName = TString(mc->GetPdf()->GetName()) + "_hesse";
    if (mc->GetWS()->pdf(hessePdfName))
    {
      spdlog::info("Optimize initial values of variables using Hesse pdf");
      minimizeHessePdf(mc->GetWS()->pdf(hessePdfName));
    }
    else
    {
      spdlog::error("Required variable initial value optimization cannot be performed as the Hesse pdf is missing. Use current values in workspace");
      _fitHesse = false;
    }
  }

  // -----------------------------------------------------------------------------------------------------
  setBinnedLHAttr(mc);
  if (_GKIntegrator)
    RooAbsReal::defaultIntegratorConfig()->method1D().setLabel("RooAdaptiveGaussKronrodIntegrator1D"); // Better numerical integrator
  unique_ptr<RooAbsReal> nll(createNLL(mc, data));

  minimize(nll.get(), mc);

  if (_outputFile != "")
  {
    spdlog::info("Saving results to {}", _outputFile.Data());
    // Create output file and save fit results
    TFile *fout = new TFile(_outputFile, "RECREATE");
    fout->cd();

    if (_saveFitResult && (_status != -1))
    {
      _result.get()->Write(); // zirui
    }
    // Get important values to save
    double nllVal = nll->getVal();
    std::map<std::string, double> muMap, NPMap;
    std::map<std::string, double> muErrorUpMap;
    std::map<std::string, double> muErrorDownMap;
    for (RooLinkedListIter it = _paramList->iterator(); RooRealVar *POI = dynamic_cast<RooRealVar *>(it.Next());)
    {
      muMap[POI->GetName()] = POI->getVal();
      if (_saveErrors)
      {
        muErrorUpMap[POI->GetName()] = POI->getErrorHi();
        muErrorDownMap[POI->GetName()] = POI->getErrorLo();
      }
    }

    // Save values to TTree
    TTree *nllTree = new TTree("nllscan", "nllscan");
    nllTree->Branch("status", &_status);
    nllTree->Branch("nll", &nllVal);
    for (RooLinkedListIter it = _paramList->iterator(); RooRealVar *POI = dynamic_cast<RooRealVar *>(it.Next());)
    {
      nllTree->Branch(POI->GetName(), &(muMap[POI->GetName()]));
      if (_saveErrors)
      {
        nllTree->Branch((std::string(POI->GetName()) + "__up").c_str(), &(muErrorUpMap[POI->GetName()]));
        nllTree->Branch((std::string(POI->GetName()) + "__down").c_str(), &(muErrorDownMap[POI->GetName()]));
      }
    }

    if (_saveNP)
    {
      for (RooLinkedListIter it = mc->GetNuisanceParameters()->iterator(); RooRealVar *NP = dynamic_cast<RooRealVar *>(it.Next());)
      {
        NPMap[NP->GetName()] = NP->getVal();
        nllTree->Branch(NP->GetName(), &(NPMap[NP->GetName()]));
      }
    }
    nllTree->Fill();
    nllTree->Write();
    if (_saveWS)
    {
      RooArgSet everything;
      utils::collectEverything(mc, &everything);
      mc->GetWS()->saveSnapshot(_ssName, everything);
      mc->GetWS()->Write();
    }
    fout->Close();
  }
  return _status;
}

void fitTool::minimize(RooAbsReal *nll, ModelConfig *mc)
{
  RooMinimizer minim(*nll);
  if (_errorLevel > 0)
    minim.setErrorLevel(_errorLevel);
  minim.setStrategy(_minStrat);
  minim.setPrintLevel(_printLevel - 1);
  if (_printLevel < 0)
    RooMsgService::instance().setGlobalKillBelow(RooFit::FATAL);
  minim.setProfile(); /* print out time */
  minim.setEps(_minTolerance / 0.001);
  minim.setOffsetting(_nllOffset);
  if (_optConst > 0)
    minim.optimizeConst(_optConst);

  // Line suggested by Stefan, to avoid running out function calls when there are many parameters
  minim.setMaxFunctionCalls(5000 * mc->GetPdf()->getVariables()->getSize());

  _status = 0;

  if (_useSIMPLEX)
  {
    spdlog::info("Starting fit with SIMPLEX...");
    _status += minim.simplex();
  }

  minim.setMinimizerType(_minAlgo.Data());
  // Perform fit with MIGRAD
  _status += minim.minimize(_minAlgo);

  if (_useHESSE)
  {
    spdlog::info("Starting fit with HESSE...");
    _status += minim.hesse();
    std::unique_ptr<RooFitResult> res_hesse(minim.save("hesse", ""));
    res_hesse->Print();
    if (_saveHesse)
      createMultiGaus(mc, res_hesse.get());
  }

  // Copied from RooAbsPdf::fitTo()
  if (_doSumW2 == 1 && minim.getNPar() > 0)
  {
    spdlog::info("Evaluating SumW2 error...");
    // Make list of RooNLLVar components of FCN
    std::unique_ptr<RooArgSet> comps(nll->getComponents());
    std::vector<RooNLLVar *> nllComponents;
    nllComponents.reserve(comps->getSize());
    std::unique_ptr<TIterator> citer(comps->createIterator());
    RooAbsArg *arg;
    while ((arg = (RooAbsArg *)citer->Next()))
    {
      RooNLLVar *nllComp = dynamic_cast<RooNLLVar *>(arg);
      if (!nllComp)
        continue;
      nllComponents.push_back(nllComp);
    }

    // Calculated corrected errors for weighted likelihood fits
    std::unique_ptr<RooFitResult> rw(minim.save());
    for (vector<RooNLLVar *>::iterator it = nllComponents.begin(); nllComponents.end() != it; ++it)
    {
      (*it)->applyWeightSquared(kTRUE);
    }
    spdlog::info("Calculating sum-of-weights-squared correction matrix for covariance matrix");
    minim.hesse();
    std::unique_ptr<RooFitResult> rw2(minim.save());
    for (vector<RooNLLVar *>::iterator it = nllComponents.begin(); nllComponents.end() != it; ++it)
    {
      (*it)->applyWeightSquared(kFALSE);
    }

    // Apply correction matrix
    const TMatrixDSym &matV = rw->covarianceMatrix();
    TMatrixDSym matC = rw2->covarianceMatrix();
    using ROOT::Math::CholeskyDecompGenDim;
    CholeskyDecompGenDim<Double_t> decomp(matC.GetNrows(), matC);
    if (!decomp)
    {
      spdlog::error("Cannot apply sum-of-weights correction to covariance matrix: correction matrix calculated with weight-squared is singular");
    }
    else
    {
      // replace C by its inverse
      decomp.Invert(matC);
      // the class lies about the matrix being symmetric, so fill in the
      // part above the diagonal
      for (int i = 0; i < matC.GetNrows(); ++i)
        for (int j = 0; j < i; ++j)
          matC(j, i) = matC(i, j);
      matC.Similarity(matV);
      // C now contiains V C^-1 V
      // Propagate corrected errors to parameters objects
      minim.applyCovarianceMatrix(matC);
    }
  }

  if (_useMINOS > 0)
  {
    spdlog::info("Starting fit with MINOS...");
    if (_useMINOS == 1)
    {
      spdlog::info("Evaluating MINOS errors for all POIs...");
      _status += minim.minos(*_paramList);
    }
    else if (_useMINOS == 2)
    {
      spdlog::info("Evaluating MINOS errors for all NPs...");
      _status += minim.minos(*mc->GetNuisanceParameters());
    }
    else if (_useMINOS == 3)
    {
      spdlog::info("Evaluating MINOS errors for both POIs and NPs...");
      _status += minim.minos(RooArgSet(*_paramList, *mc->GetNuisanceParameters()));
    }
    else
    {
      spdlog::error("Unknown code for MINOS fit {}. Assume running MINOS only on POIs...", _useMINOS);
      _status += minim.minos(*_paramList);
    }
  }

  if (_saveFitResult)
    _result.reset(minim.save("fitResult", "Fit Results"));
}

void fitTool::setBinnedLHAttr(ModelConfig *mc)
{
  RooWorkspace *w = mc->GetWS();

  RooArgSet funcs = w->allPdfs();
  std::unique_ptr<TIterator> iter(funcs.createIterator());
  for (RooAbsPdf *v = (RooAbsPdf *)iter->Next(); v != 0; v = (RooAbsPdf *)iter->Next())
  {
    std::string name = v->GetName();
    if (v->IsA() == RooRealSumPdf::Class())
    {
      spdlog::info("Set binned likelihood for: {}", v->GetName());
      v->setAttribute("BinnedLikelihood", true);
    }
  }
}

RooAbsReal *fitTool::createNLL(ModelConfig *mc, RooAbsData *data)
{
  RooAbsPdf *pdf = mc->GetPdf();
  TStopwatch timer1;
  spdlog::info("Building NLL...");
  // RooAbsReal *nll = pdf->createNLL(*data, NumCPU(_nCPU,3),
  if (_fixStarCache)
    utils::fixRooStarCache(mc->GetWS());
#if ROOT_VERSION_CODE >= ROOT_VERSION(6, 24, 3)
  RooAbsReal *nll = nullptr;
  const RooArgSet *condObs = mc->GetConditionalObservables();
  if (condObs) {
    nll = pdf->createNLL(*data, NumCPU(_nCPU), BatchMode(_useBatch), IntegrateBins(_samplingRelTol),
                         Constrain(*mc->GetNuisanceParameters()),
                         GlobalObservables(*mc->GetGlobalObservables()),
                         ConditionalObservables(*condObs),
                         ExternalConstraints(*_externalConstraint));
  }
  else {
    nll = pdf->createNLL(*data, NumCPU(_nCPU), BatchMode(_useBatch), IntegrateBins(_samplingRelTol),
                         Constrain(*mc->GetNuisanceParameters()),
                         GlobalObservables(*mc->GetGlobalObservables()),
                         ExternalConstraints(*_externalConstraint));
  }
#else
  RooAbsReal *nll = NULL;
  if(_rangeName != "")
    nll = pdf->createNLL(*data, NumCPU(_nCPU),
                         Constrain(*mc->GetNuisanceParameters()),
                         GlobalObservables(*mc->GetGlobalObservables()),
                         ConditionalObservables(*mc->GetConditionalObservables()),
                         ExternalConstraints(*_externalConstraint), Range(_rangeName), SplitRange());
  else
    nll = pdf->createNLL(*data, NumCPU(_nCPU),
                         Constrain(*mc->GetNuisanceParameters()),
                         GlobalObservables(*mc->GetGlobalObservables()),
                         ConditionalObservables(*mc->GetConditionalObservables()),
                         ExternalConstraints(*_externalConstraint));
#endif
  if (_fixStarCache)
    utils::fixRooStarCache(mc->GetWS()); // needs to be done twice.
  nll->enableOffsetting(_nllOffset);

  timer1.Stop();
  double t_cpu_ = timer1.CpuTime() / 60.;
  double t_real_ = timer1.RealTime() / 60.;
  spdlog::info("NLL built in {} min (cpu), {} min (real)", t_cpu_, t_real_);
  return nll;
}

void fitTool::setStorageLevel(int level)
{
  if (_minAlgo != "Minuit2")
  {
    spdlog::warn("Storage level can only be set for Minuit2");
    return;
  }
  ROOT::Math::IOptions *opt = &ROOT::Math::MinimizerOptions::Default(_minAlgo);
  opt->SetValue("StorageLevel", level);
  ROOT::Math::MinimizerOptions::SetDefaultExtraOptions(opt);
}

void fitTool::setSaveHesse(bool flag)
{
  _saveHesse = flag;

  // Sanity checks
  if (_saveHesse)
  {
    if (_outputFile == "")
      auxUtils::alertAndAbort("Request saving Hesse pdf in output file while output file name is not provided");

    if(!_saveWS)
    {
      spdlog::warn("Request saving Hesse pdf in output file. Enable workspace saving");
      _saveWS = true;
    }

    if(!_useHESSE)
    {
      spdlog::warn("Request saving Hesse pdf in output file. Enable hesse algorithm");
      _useHESSE = true;
    }
  }
}

void fitTool::createMultiGaus(ModelConfig *mc, RooFitResult *res)
{
  TString pdfName = TString(mc->GetPdf()->GetName()) + "_hesse";
  std::unique_ptr<RooAbsPdf> gaus(res->createHessePdf(res->floatParsFinal()));
  gaus->SetName(pdfName);
  mc->GetWS()->import(*gaus);
  spdlog::info("Hesse pdf {} saved in workspace {}", pdfName.Data(), mc->GetWS()->GetName());
}

double fitTool::minimizeHessePdf(RooAbsPdf *pdf)
{
  RooFormulaVar nll("nll", "-log(@0)", RooArgList(*pdf));

  RooMinimizer minim(nll);
  minim.setErrorLevel(0.5);
  minim.setStrategy(1);
  minim.setPrintLevel(0);
  minim.setProfile();
  minim.setEps(1e-3);
  minim.setOffsetting(0);
  minim.optimizeConst(2);
  minim.setMinimizerType("Minuit2");
  minim.minimize("Minuit2", "Migrad");

  return 2*nll.getVal();
}

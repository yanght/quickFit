#include "auxUtils.h"

string auxUtils::OKGREEN = "\033[92m";
string auxUtils::FAIL = "\033[91m";
string auxUtils::ENDC = "\033[0m";

std::vector<std::string> auxUtils::Tokenize(const std::string& str, const std::string& delimiters) {
  std::vector<std::string> tokens;

  // Skip delimiters at beginning.
  std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
  // Find first "non-delimiter".
  std::string::size_type pos     = str.find_first_of(delimiters, lastPos);

  while (std::string::npos != pos || std::string::npos != lastPos) {
    // Found a token, add it to the vector.
    tokens.push_back(str.substr(lastPos, pos - lastPos));
    // Skip delimiters.  Note the "not_of"
    lastPos = str.find_first_not_of(delimiters, pos);
    // Find next "non-delimiter"
    pos = str.find_first_of(delimiters, lastPos);
  }
  return tokens;
}

vector<TString> auxUtils::splitString(const TString& theOpt, const char separator ){
  // splits the option string at 'separator' and fills the list
  // 'splitV' with the primitive strings
  vector<TString> splitV;
  TString splitOpt(theOpt);
  splitOpt.ReplaceAll("\n"," ");
  splitOpt = splitOpt.Strip(TString::kBoth,separator);
  while (splitOpt.Length()>0) {
    if ( !splitOpt.Contains(separator) ) {
      splitV.push_back(splitOpt);
      break;
    }
    else {
      TString toSave = splitOpt(0,splitOpt.First(separator));
      splitV.push_back(toSave);
      splitOpt = splitOpt(splitOpt.First(separator),splitOpt.Length());
    }
    splitOpt = splitOpt.Strip(TString::kLeading,separator);
  }
  return splitV;
}
  
void auxUtils::printTitle(TString titleText, TString separator, int width){
  TString fullLine="", line="";
  
  int stringLength=titleText.Length();
  int fullLineWidth=2*width+((stringLength>2*width) ? (stringLength) : (2*width))+2;
    
  for(int i=0;i<fullLineWidth;i++) fullLine+=separator;
  for(int i=0;i<width;i++) line+=separator;
  
  cout<<endl<<TString("\t "+fullLine)<<endl;
  if(stringLength>2*width) cout<<"\t "<<line<<" "<<titleText<<" "<<line<<endl;
  else printf("\t "+line+" %*s%*s "+line+"\n",int(width+titleText.Length()/2),titleText.Data(),int(width-titleText.Length()/2),"");
  cout<<TString("\t "+fullLine)<<endl<<endl;
}

TXMLAttr *auxUtils::findAttribute(TXMLNode* rootNode, TString attributeKey){
  TListIter attribIt = rootNode->GetAttributes();
  TXMLAttr* curAttr = 0;

  while (( curAttr = dynamic_cast< TXMLAttr* >( attribIt() ) ) != 0 )
    if ( curAttr->GetName() == attributeKey ) return curAttr;

  return NULL;
}

void auxUtils::removeWhiteSpace(TString& item){
  std::string s=item.Data();
  s.erase( std::remove_if( s.begin(), s.end(), std::bind( std::isspace<char>, std::placeholders::_1, std::locale::classic() ) ), s.end() );
  item=s.c_str();
}

void auxUtils::alertAndAbort(TString msg){
  cerr<<"\n\033[91m \tERROR: "<<msg<<". Please intervene... \033[0m\n"<<endl;
  exit(-1);
}

TString auxUtils::getAttributeValue( TXMLNode* rootNode, TString attributeKey, bool allowEmpty, TString defaultStr){
  TXMLAttr* attr = findAttribute(rootNode, attributeKey);
  TString attributeValue;
  
  if(!attr){
    if(allowEmpty) attributeValue=defaultStr;
    else alertAndAbort("Attribute "+attributeKey+" cannot be found in node "+rootNode->GetNodeName());
  }
  else attributeValue = attr->GetValue();

  removeWhiteSpace(attributeValue);
  return attributeValue;
}




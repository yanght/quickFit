#include "utils.h"
#include "RooFitExtensions/RooStarMomentMorph.h"

#include "spdlog/spdlog.h"

void utils::collectEverything(ModelConfig *mc, RooArgSet *set)
{
  if (mc->GetNuisanceParameters())
    set->add(*mc->GetNuisanceParameters());
  else
  {
    spdlog::warn("No nuisance parameter set defined in ModelConfig. Use an empty set...");
    mc->SetNuisanceParameters(RooArgSet());
  }
  if (mc->GetGlobalObservables())
    set->add(*mc->GetGlobalObservables());
  else
  {
    spdlog::warn("WARNING: No global observable set defined in ModelConfig. Use an empty set...");
    mc->SetGlobalObservables(RooArgSet());
  }
  if (mc->GetParametersOfInterest())
    set->add(*mc->GetParametersOfInterest());
  else
  {
    spdlog::error("No parameter of interest set defined in ModelConfig. Aborting...");
    abort();
  }
}

void utils::fixRooStarCache(RooWorkspace *ws)
{
  RooFIter iter = ws->components().fwdIterator();
  RooAbsArg *arg;
  while ((arg = iter.next()))
  {
    if (arg->IsA() == RooStarMomentMorph::Class())
    {
      ((RooStarMomentMorph *)arg)->fixCache();
    }
  }
}

void utils::Reset(RooArgSet *original, RooArgSet *snapshot)
{
  *original = *snapshot;
  // Still need to recover the ranges for variables
  std::unique_ptr<TIterator> iter(original->createIterator());
  RooRealVar *parg = NULL;
  while ((parg = dynamic_cast<RooRealVar *>(iter->Next())))
  {
    RooRealVar *snapVar = dynamic_cast<RooRealVar *>(snapshot->find(parg->GetName()));
    parg->setRange(snapVar->getMin(), snapVar->getMax());
  }
}

void utils::setValAndFix(RooRealVar *var, double value)
{
  if (var->getMax() < value)
  {
    var->setMax(value + 1);
  }
  else if (var->getMin() > value)
  {
    var->setMin(value - 1);
  }
  var->setVal(value);
  var->setConstant(true);
}

void utils::randomizeSet(RooAbsPdf *pdf, RooArgSet *globs, int seed)
{
  if (seed >= 0)
    RooRandom::randomGenerator()->SetSeed(seed); // This step is necessary
  std::unique_ptr<RooDataSet> pseudoGlobals(pdf->generateSimGlobal(*globs, 1));
  std::unique_ptr<RooArgSet> allVars(pdf->getVariables());
  allVars->assignValueOnly(*pseudoGlobals->get(0));
}

RooRealVar *utils::setupPOI(ModelConfig *mc, const std::string _poiStr, RooArgSet *fitPOIs, bool mustFloat)
{
  RooRealVar *firstPOI = NULL;
  RooWorkspace *ws = mc->GetWS();
  spdlog::info("Preparing parameters of interest: {}", _poiStr.c_str());
  std::vector<std::string> poiStrs_temp = auxUtils::Tokenize(_poiStr, ","), poiStrs;

  // Replace wildcard
  for (auto poiSetup : poiStrs_temp)
  {
    if (poiSetup.find('*') != std::string::npos)
    {
      // poiStrs.erase(std::remove(poiStrs.begin(), poiStrs.end(), poiSetup), poiStrs.end());
      std::vector<std::string> poiTerms = auxUtils::Tokenize(poiSetup, "=");
      TString poiNameWild = (TString)poiTerms[0];
      RooAbsCollection *poiList = mc->GetWS()->allVars().selectByName(poiNameWild);
      for (RooLinkedListIter it = poiList->iterator(); RooRealVar *poi = dynamic_cast<RooRealVar *>(it.Next());)
      {
        TString poiName = poi->GetName();
        TString newSetup = poiTerms.size() > 1 ? poiName + "=" + poiTerms[1] : poiName;
        poiStrs.push_back(newSetup.Data());
        // cout<<"Adding "<<newSetup<<endl;
      }
    }
    else
      poiStrs.push_back(poiSetup);
  }

  for (unsigned int ipoi(0); ipoi < poiStrs.size(); ipoi++)
  {
    std::vector<std::string> poiTerms = auxUtils::Tokenize(poiStrs[ipoi], "=");
    TString poiName = (TString)poiTerms[0];

    // check if variable is in workspace
    if (not ws->var(poiName))
    {
      spdlog::warn("---> POI {} not in workspace. Skipping...", poiName.Data());
      continue;
    }

    // set variable for fit
    fitPOIs->add(*(ws->var(poiName)), true);
    if (poiTerms.size() > 1)
    {
      std::vector<std::string> poiVals = auxUtils::Tokenize(poiTerms[1], "_");
      if (poiVals.size() == 3)
      { // Specify range and central value
        ws->var(poiName)->setRange(std::stof(poiVals[1]), std::stof(poiVals[2]));
        ws->var(poiName)->setVal(std::stof(poiVals[0]));
        ws->var(poiName)->setConstant(kFALSE);
      }
      else if (poiVals.size() == 2)
      { // Specify only range
        ws->var(poiName)->setRange(std::stof(poiVals[0]), std::stof(poiVals[1]));
        ws->var(poiName)->setConstant(kFALSE);
      }
      else if (poiVals.size() == 1)
      { // Fix the parameter to current value
        if (std::stof(poiVals[0]) > ws->var(poiName)->getMax())
        {
          ws->var(poiName)->setRange(ws->var(poiName)->getMin(), 2 * std::stof(poiVals[0]));
        }
        if (std::stof(poiVals[0]) < ws->var(poiName)->getMin())
        {
          ws->var(poiName)->setRange(-2 * abs(std::stof(poiVals[0])), ws->var(poiName)->getMax());
        }
        ws->var(poiName)->setVal(std::stof(poiVals[0]));
        ws->var(poiName)->setConstant(kTRUE);
      }
    }
    else
    {
      ws->var(poiName)->setConstant(kFALSE);
    }

    // ws->var(poiName)->Print();
    if (!firstPOI && !ws->var(poiName)->isConstant())
    {
      firstPOI = ws->var(poiName);
      spdlog::info("---> Set first POI to {}", firstPOI->GetName());
    }
  }

  fitPOIs->add(*mc->GetParametersOfInterest(), true);
  if (fitPOIs->getSize() == 0)
  {
    spdlog::error("No POI specificied or provided. Aborting...");
    abort();
  }

  if (!firstPOI && mustFloat)
  {
    firstPOI = (RooRealVar *)mc->GetParametersOfInterest()->first();
    spdlog::info("Float the first POI in the ModelConfig: {}", firstPOI->GetName());
    firstPOI->setConstant(kFALSE);
    firstPOI->Print();
  }
  return firstPOI;
}

void utils::loadSnapshot(RooWorkspace *ws, const std::string _snapshot)
{
  if (_snapshot != "")
  {
    vector<std::string> snapshots = auxUtils::Tokenize(_snapshot, ",");
    for (auto snapshot : snapshots)
    {
      spdlog::info("Loading snapshot {}", snapshot.c_str());
      if (not ws->loadSnapshot(snapshot.c_str()))
      {
        spdlog::error("Unable to load snapshot {} from workspace. Aborting...", snapshot.c_str());
        abort();
      }
    }
  }
}

void utils::fixNP(ModelConfig *mc, const std::string _fixNPStr)
{
  spdlog::info("Fixing nuisance parameters {}:", _fixNPStr.c_str());
  std::vector<std::string> fixNPStrs = auxUtils::Tokenize(_fixNPStr, ",");
  for (unsigned int inp(0); inp < fixNPStrs.size(); inp++)
  {
    RooAbsCollection *fixNPs = mc->GetNuisanceParameters()->selectByName(fixNPStrs[inp].c_str());
    for (RooLinkedListIter it = fixNPs->iterator(); RooRealVar *NP = dynamic_cast<RooRealVar *>(it.Next());)
    {
      spdlog::info("---> Fixing nuisance parameter {} to value {}", NP->GetName(), NP->getVal());
      NP->setConstant(kTRUE);
    }
  }
}

void utils::fixAtt(ModelConfig *mc, const std::string _fixAttStr)
{
  spdlog::info("Fixing nuisance parameters with attribute {}:", _fixAttStr.c_str());
  for (RooLinkedListIter it = mc->GetNuisanceParameters()->iterator(); RooRealVar *NP = dynamic_cast<RooRealVar *>(it.Next());)
  {
    if (NP->getAttribute(_fixAttStr.c_str()))
    {
      spdlog::info("---> Fixing nuisance parameter {} to value {}", NP->GetName(), NP->getVal());
      NP->setConstant(kTRUE);
    }
  }
}

void utils::setExtConstr(RooWorkspace *ws, const std::string _externalConstraint, RooArgSet *externalConstrSet)
{
  spdlog::info("Including external constraints:");
  std::vector<std::string> externalConstrStrs = auxUtils::Tokenize(_externalConstraint, ",");
  for (auto externalConstrName : externalConstrStrs)
    if (ws->pdf(externalConstrName.c_str()))
    {
      spdlog::info("---> Adding external constraint {}", externalConstrName.c_str());
      externalConstrSet->add(*ws->pdf(externalConstrName.c_str()));
    }
    else
      spdlog::error("---> External constraint {} not in workspace. Skipping...", externalConstrName.c_str());
}